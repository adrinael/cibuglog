.. CI Bug Log documentation master file, created by
   sphinx-quickstart on Thu Mar 12 15:31:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../README.md

Overview of the models
======================

.. image:: CIResults/uml.svg
  :width: 800
  :alt: Overview of CI Bug Log's models

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   CIResults/CIResults.rst
   replication/replication.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
