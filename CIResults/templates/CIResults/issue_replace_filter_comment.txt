{% load helpers %}{% autoescape off %}A CI Bug Log filter associated to this bug has been updated by {{user.get_full_name}}:

**Description:** {{old_filter.description|markdown_diff:new_filter.description}}

**Equivalent query:** {{old_filter.to_user_query.query|markdown_diff:new_filter.to_user_query.query}}

{% if new_matched_failures|length > 0 %}New failures caught by the filter:
{% for failure in new_matched_failures %}
{% if failure.result.url %}  * {{failure.result.url}}{% endif %}{% endfor %}{% else %}
  No new failures caught with the new filter{% endif %}{% endautoescape %}
