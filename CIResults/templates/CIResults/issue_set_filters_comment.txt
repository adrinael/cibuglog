{% load humanize %}The CI Bug Log issue associated to this bug has been updated by {{user.get_full_name}}.
{% if removed_ifas|length > 0 %}
### Removed filters
{% for ifa in removed_ifas %}
* {{ifa.filter.description}} (added on {{ifa.filter.added_on|naturaltime}}){% endfor %}
{% endif %}{% if new_filters|length > 0 %}
### New filters associated
{% for new_filter, new_failures in new_filters.items %}
* {{new_filter.description}}{% for failure in new_failures %}
{% if failure.result.url %}  - {{failure.result.url}}{% endif %}{% empty %}
  (No new failures associated){% endfor %}
{% endfor %}{% endif %}
