from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.contrib.postgres.fields import JSONField
from django.utils.functional import cached_property
from django.db import models, transaction
from django.utils.timesince import timesince
from django.utils import timezone
from django.db.models import Q
from django.template.loader import render_to_string

from .runconfigdiff import RunConfigDiff
from .filtering import UserFiltrableMixin, FilterObjectStr, FilterObjectDateTime
from .filtering import FilterObjectDuration, FilterObjectBool, FilterObjectInteger
from .filtering import FilterObjectModel, FilterObjectJSON
from .sandbox.io import Client

from collections import defaultdict, OrderedDict

from datetime import timedelta
import hashlib
import traceback
import re


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='<deleted>',
                                                  defaults={'email': 'deleted.user@admin',
                                                            'last_login': timezone.now()})[0].id


class ColoredObjectMixin:
    color_hex = models.CharField(max_length=7, null=True, blank=True,
                                 help_text="Color that should be used to represent this object, in hex format. "
                                           "Use https://www.w3schools.com/colors/colors_picker.asp to pick a color.",
                                 validators=[RegexValidator(
                                     regex='^#[0-9a-f]{6}$',
                                     message='Not a valid hex color format (example: #89ab13)',
                                 )])

    @cached_property
    def color(self):
        if self.color_hex is not None:
            return self.color_hex

        # Generate a random color
        blake2 = hashlib.blake2b()
        blake2.update(self.name.encode())
        return "#" + blake2.hexdigest()[-7:-1]


# Bug tracking


class BugTrackerSLA(models.Model):
    tracker = models.ForeignKey("BugTracker", on_delete=models.CASCADE)
    priority = models.CharField(max_length=50, help_text="Name of the priority you want to define the "
                                                         "SLA for (case incensitive)")
    SLA = models.DurationField(help_text="Expected SLA for this priority")

    class Meta:
        unique_together = ('tracker', 'priority')
        verbose_name_plural = "Bug Tracker SLAs"

    def __str__(self):
        return "{}: {} -> {}".format(str(self.tracker), self.priority, naturaltime(self.SLA))


class Person(models.Model):
    full_name = models.CharField(max_length=100, help_text="Full name of the person", blank=True, null=True)
    email = models.EmailField(null=True)

    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        has_full_name = self.full_name is not None and len(self.full_name) > 0
        has_email = self.email is not None and len(self.email) > 0

        if has_full_name and has_email:
            return "{} <{}>".format(self.full_name, self.email)
        elif has_full_name:
            return self.full_name
        elif has_email:
            return self.email
        else:
            return "(No name or email)"


class BugTrackerAccount(models.Model):
    tracker = models.ForeignKey("BugTracker", on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    user_id = models.CharField(max_length=254, help_text="User ID on the bugtracker")

    is_developer = models.BooleanField(help_text="Is this a developer's account?")

    class Meta:
        unique_together = ('tracker', 'user_id')

    def __str__(self):
        return str(self.person)


class BugTracker(models.Model):
    """ Represents a bug tracker such as Bugzilla or Jira """

    name = models.CharField(max_length=50, unique=True, help_text="Full name of the bugtracker (e.g. Freedesktop.org)")
    short_name = models.CharField(max_length=10, help_text="Very shorthand name (e.g. fdo)")
    project = models.CharField(max_length=50, blank=True, null=True,
                               help_text="Specific project key in bugtracker to use (e.g. "
                                         "gitlab (project id): 1234 "
                                         "bugzilla (product/component): Mesa/EGL"
                                         "jira (project key): TEST)")
    separator = models.CharField(max_length=1,
                                 help_text="Separator to construct a shorthand of the bug (e.g. '#' to get fdo#1234)")
    public = models.BooleanField(help_text="Should bugs filed on this tracker be visible on the public website?")

    url = models.URLField(help_text="Public URL to the bugtracker (e.g. "
                                    "gitlab: https://gitlab.freedesktop.org "
                                    "bugzilla: https://bugs.freedesktop.org)")

    bug_base_url = models.URLField(help_text="Base URL for constructing (e.g. "
                                             "gitlab: https://gitlab.freedesktop.org/cibuglog/cibuglog/issues/ "
                                             "bugzilla: https://bugs.freedesktop.org/show_bug.cgi?id=)")

    tracker_type = models.CharField(max_length=50, help_text="Legal values: bugzilla, gitlab, jira, untracked")
    username = models.CharField(max_length=50, blank=True,
                                help_text="Username to connect to the bugtracker. "
                                          "Leave empty if the bugs are public or if you do not to post comments.")
    password = models.CharField(max_length=50, blank=True,
                                help_text="Password to connect to the bugtracker. "
                                          "Leave empty if the bugs are public or if you do not to post comments.")
    # Stats
    polled = models.DateTimeField(null=True, blank=True,
                                  help_text="Last time the bugtracker was polled. To be filled automatically.")

    # Configurations
    components_followed = models.CharField(max_length=255, blank=True, null=True,
                                           help_text="Coma-separated list of components you would like to "
                                                     "keep track of. On Gitlab, specify the list of labels an issue "
                                                     "needs to have leave empty if you want all issues.")
    components_followed_since = models.DateTimeField(blank=True, null=True,
                                                     help_text="Poll bugs older than this date. WARNING: "
                                                               "Run poll_all_bugs.py after changing this date.")
    first_response_SLA = models.DurationField(help_text="Time given to developers to provide the first "
                                              "response after its creation", default=timedelta(days=1))
    custom_fields_map = JSONField(null=True, blank=True,
                                  help_text="Mapping of custom_fields that should be included when polling Bugs from "
                                            "this BugTracker, e.g. "
                                            "{'customfield_xxxx': 'severity', 'customfield_yyyy': 'build_number'}. "
                                            "If a customfield mapping corresponds to an existing Bug field, e.g. "
                                            "severity, the corresponding Bug field will be populated. If not, e.g. "
                                            "build_number, this will be populated in the Bug's custom_fields "
                                            "field. (Leave empty if not using custom fields)")

    @cached_property
    def SLAs_cached(self):
        slas = dict()
        for sla_entry in BugTrackerSLA.objects.filter(tracker=self):
            slas[sla_entry.priority.lower()] = sla_entry.SLA
        return slas

    @cached_property
    def tracker(self):
        from .bugtrackers import Untracked, Bugzilla, Jira, GitLab

        if self.tracker_type == "bugzilla":
            return Bugzilla(self)
        elif self.tracker_type == "gitlab":
            return GitLab(self)
        elif self.tracker_type == "jira":
            return Jira(self)
        elif self.tracker_type == "jira_untracked" or self.tracker_type == "untracked":
            return Untracked(self)
        else:
            raise ValueError("The bugtracker type '{}' is unknown".format(self.tracker_type))

    def poll(self, bug, force_polling_comments=False):
        self.tracker.poll(bug, force_polling_comments)
        bug.polled = timezone.now()

    def poll_all(self, stop_event=None, bugs=None):
        if bugs is None:
            bugs = Bug.objects.filter(tracker=self)

        for bug in bugs:
            # Make sure the event object did not signal us to stop
            if stop_event is not None and stop_event.is_set():
                return

            try:
                self.poll(bug)
            except Exception:                                 # pragma: no cover
                print("{} could not be polled:".format(bug))  # pragma: no cover
                traceback.print_exc()                         # pragma: no cover

            bug.save()

        # We do not catch any exceptions, so if we reach this point, it means
        # all bugs have been updated.
        self.polled = timezone.now()
        self.save()

    @property
    def tracker_time(self):
        return self.tracker._get_tracker_time()

    def to_tracker_tz(self, dt):
        return self.tracker._to_tracker_tz(dt)

    @property
    def open_statuses(self):
        return self.tracker.open_statuses

    def is_bug_open(self, bug):
        return bug.status in self.open_statuses

    @property
    def components_followed_list(self):
        if self.components_followed is None:
            return []
        else:
            return [c.strip() for c in self.components_followed.split(',')]

    @transaction.atomic
    def get_or_create_bugs(self, ids):
        new_bugs = set()

        known_bugs = Bug.objects.filter(tracker=self, bug_id__in=ids)
        known_bug_ids = set([b.bug_id for b in known_bugs])
        for bug_id in ids - known_bug_ids:
            new_bugs.add(Bug.objects.create(tracker=self, bug_id=bug_id))

        return set(known_bugs).union(new_bugs)

    def __set_tracker_to_bugs__(self, bugs):
        for bug in bugs:
            bug.tracker = self
        return bugs

    # WARNING: Some bugs may not have been polled yet
    def open_bugs(self):
        open_bugs = set(Bug.objects.filter(tracker=self, status__in=self.open_statuses))

        if not self.tracker.has_components or len(self.components_followed_list) > 0:
            open_bug_ids = self.tracker.search_bugs_ids(components=self.components_followed_list,
                                                        status=self.open_statuses)
            open_bugs.update(self.get_or_create_bugs(open_bug_ids))

        return self.__set_tracker_to_bugs__(open_bugs)

    def bugs_in_issues(self):
        # Get the list of bugs from this tracker associated to active issues
        bugs_in_issues = set()
        for issue in Issue.objects.filter(archived_on=None).prefetch_related('bugs__tracker'):
            for bug in issue.bugs.filter(tracker=self):
                bugs_in_issues.add(bug)

        return bugs_in_issues

    def followed_bugs(self):
        return self.__set_tracker_to_bugs__(self.open_bugs() | self.bugs_in_issues())

    def updated_bugs(self):
        if not self.polled:
            return self.followed_bugs()

        all_bug_ids = set(Bug.objects.filter(tracker=self).values_list('bug_id', flat=True))

        td = self.tracker_time - timezone.now()
        polled_time = self.to_tracker_tz(self.polled + td)
        all_upd_bug_ids = self.tracker.search_bugs_ids(components=self.components_followed_list,
                                                       updated_since=polled_time)
        not_upd_ids = all_bug_ids - all_upd_bug_ids

        open_bug_ids = set()
        if not self.tracker.has_components or len(self.components_followed_list) > 0:
            open_bug_ids = self.tracker.search_bugs_ids(components=self.components_followed_list,
                                                        status=self.open_statuses)

        open_db_bug_ids = set(Bug.objects.filter(tracker=self,
                                                 status__in=self.open_statuses).values_list('bug_id', flat=True))
        issue_bugs_ids = set(map(lambda x: x.bug_id, self.bugs_in_issues()))

        bug_ids = (open_bug_ids | open_db_bug_ids | issue_bugs_ids) - not_upd_ids
        upd_bugs = self.get_or_create_bugs(bug_ids)

        return self.__set_tracker_to_bugs__(upd_bugs)

    def clean(self):
        if self.custom_fields_map is None:
            return

        for field in self.custom_fields_map:
            if self.custom_fields_map[field] is None:
                continue
            self.custom_fields_map[field] = str(self.custom_fields_map[field])

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Bug(models.Model, UserFiltrableMixin):
    filter_objects_to_db = {
        'filter_description':
            FilterObjectStr('issue__filters__description',
                            'Description of what the filter associated to an issue referencing this bug matches'),
        'filter_runconfig_tag_name':
            FilterObjectStr('issue__filters__tags__name',
                            'Run configuration tag matched by the filter associated to an issue referencing this bug'),
        'filter_machine_tag_name':
            FilterObjectStr('issue__filters__machine_tags__name',
                            'Machine tag matched by the filter associated to an issue referencing this bug'),
        'filter_machine_name':
            FilterObjectStr('issue__filters__machines__name',
                            'Name of a machine matched by the filter associated to an issue referencing this bug'),
        'filter_test_name':
            FilterObjectStr('issue__filters__tests__name',
                            'Name of a test matched by the filter associated to an issue referencing this bug'),
        'filter_status_name':
            FilterObjectStr('issue__filters__statuses__name',
                            'Status matched by the filter associated to an issue referencing this bug'),
        'filter_stdout_regex':
            FilterObjectStr('issue__filters__stdout_regex',
                            'Standard output regex used by the filter associated to an issue referencing this bug'),
        'filter_stderr_regex':
            FilterObjectStr('issue__filters__stderr_regex',
                            'Standard error regex used by the filter associated to an issue referencing this bug'),
        'filter_dmesg_regex':
            FilterObjectStr('issue__filters__dmesg_regex',
                            'Regex for dmesg used by the filter associated to an issue referencing this bug'),
        'filter_added_on':
            FilterObjectDateTime('issue__issuefilterassociated__added_on',
                                 'Date at which the filter was associated to the issue referencing this bug'),
        'filter_covers_from':
            FilterObjectDateTime('issue__issuefilterassociated__covers_from',
                                 'Date of the first failure covered by the filter'),
        'filter_deleted_on':
            FilterObjectDateTime('issue__issuefilterassociated__deleted_on',
                                 'Date at which the filter was removed from the issue referencing this bug'),
        'filter_runconfigs_covered_count':
            FilterObjectInteger('issue__issuefilterassociated__runconfigs_covered_count',
                                'Amount of run configurations covered by the filter associated to the issue referencing this bug'),  # noqa
        'filter_runconfigs_affected_count':
            FilterObjectInteger('issue__issuefilterassociated__runconfigs_affected_count',
                                'Amount of run configurations affected by the filter associated to the issue referencing this bug'),  # noqa
        'filter_last_seen':
            FilterObjectDateTime('issue__issuefilterassociated__last_seen',
                                 'Date at which the filter associated to the issue referencing this bug last matched'),
        'filter_last_seen_runconfig_name':
            FilterObjectStr('issue__issuefilterassociated__last_seen_runconfig__name',
                            'Run configuration which last matched the filter associated to the issue referencing this bug'),  # noqa

        'issue_description': FilterObjectStr('issue__description',
                                             'Free-hand text associated to the issue by the bug filer'),
        'issue_filer_email': FilterObjectStr('issue__filer', 'Email address of the person who filed the issue'),
        'issue_added_on': FilterObjectDateTime('issue__added_on', 'Date at which the issue was created'),
        'issue_archived_on': FilterObjectDateTime('issue__archived_on', 'Date at which the issue was archived'),
        'issue_expected': FilterObjectBool('issue__expected', 'Is the issue expected?'),
        'issue_runconfigs_covered_count': FilterObjectInteger('issue__runconfigs_covered_count',
                                                              'Amount of run configurations covered by the issue'),
        'issue_runconfigs_affected_count': FilterObjectInteger('issue__runconfigs_affected_count',
                                                               'Amount of run configurations affected by the issue'),
        'issue_last_seen': FilterObjectDateTime('issue__last_seen', 'Date at which the issue was last seen'),
        'issue_last_seen_runconfig_name': FilterObjectStr('issue__last_seen_runconfig__name',
                                                          'Run configuration which last reproduced the issue'),

        'tracker_name': FilterObjectStr('tracker__name', 'Name of the tracker hosting the bug'),
        'tracker_short_name': FilterObjectStr('tracker__short_name', 'Short name of the tracker which hosts the bug'),
        'tracker_type': FilterObjectStr('tracker__tracker_type', 'Type of the tracker which hosts the bug'),
        'bug_id': FilterObjectStr('bug_id', 'ID of the bug'),
        'title': FilterObjectStr('title', 'Title of the bug'),
        'created_on': FilterObjectDateTime('created', 'Date at which the bug was created'),
        'updated_on': FilterObjectDateTime('updated', 'Date at which the bug was last updated'),
        'closed_on': FilterObjectDateTime('closed', 'Date at which the bug was closed'),
        'creator_name': FilterObjectStr('creator__person__full_name', 'Name of the creator of the bug'),
        'creator_email': FilterObjectStr('creator__person__email', 'Email address of the creator of the bug'),
        'assignee_name': FilterObjectStr('assignee__person__full_name', 'Name of the assignee of the bug'),
        'assignee_email': FilterObjectStr('assignee__person__email', 'Email address of the assignee of the bug'),
        'product': FilterObjectStr('product', 'Product of the bug'),
        'component': FilterObjectStr('component', 'Component of the bug'),
        'priority': FilterObjectStr('priority', 'Priority of the bug'),
        'features': FilterObjectStr('features', 'Features affected (coma-separated list)'),
        'platforms': FilterObjectStr('platforms', 'Platforms affected (coma-separated list)'),
        'status': FilterObjectStr('status', 'Status of the bug (RESOLVED/FIXED, ...)'),
        'tags': FilterObjectStr('tags', 'Tags/labels associated to the bug (coma-separated list)'),
        'custom_fields': FilterObjectJSON('custom_fields', 'Custom Bug fields stored by key. Access using dot'
                                                           'notation, e.g. custom_fields.build_id'),
        'parent_id': FilterObjectInteger('parent', 'ID of the parent bug from which this bug has been replicated'),
    }

    UPDATE_PENDING_TIMEOUT = timedelta(minutes=45)

    """
    Stores a single bug entry, related to :model:`CIResults.BugTracker`.
    """
    tracker = models.ForeignKey(BugTracker, on_delete=models.CASCADE,
                                help_text="On which tracker is the bug stored")
    bug_id = models.CharField(max_length=20,
                              help_text="The ID of the bug (e.g. 1234)")

    # To be updated when polling
    parent = models.ForeignKey("self", null=True, blank=True, related_name="children", on_delete=models.CASCADE,
                               help_text="This is the parent bug, if this bug is replicated. "
                               "To be filled automatically")
    title = models.CharField(max_length=500, null=True, blank=True,
                             help_text="Title of the bug report. To be filled automatically.")
    description = models.TextField(null=True, blank=True, help_text="Description of the bug report. To "
                                   "be filled automatically")
    created = models.DateTimeField(null=True, blank=True,
                                   help_text="Date of the creation of the bug report. To be filled automatically.")
    updated = models.DateTimeField(null=True, blank=True,
                                   help_text="Last update made to the bug report. To be filled automatically.")
    polled = models.DateTimeField(null=True, blank=True,
                                  help_text="Last time the bug was polled. To be filled automatically.")
    closed = models.DateTimeField(null=True, blank=True,
                                  help_text="Time at which the bug got closed. To be filled automatically.")
    creator = models.ForeignKey(BugTrackerAccount, on_delete=models.CASCADE, null=True, blank=True,
                                related_name="bug_creator_set", help_text="Person who wrote the initial bug report")
    assignee = models.ForeignKey(BugTrackerAccount, on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="bug_assignee_set", help_text="Current assignee of the bug")
    product = models.CharField(max_length=50, null=True, blank=True,
                               help_text="Product used for the bug filing (e.g. DRI) or NULL if not "
                                         "applicable. "
                                         "For GitLab, this is taken from labels matching 'product: $value'. "
                                         "To be filled automatically.")
    component = models.CharField(max_length=500, null=True, blank=True,
                                 help_text="Component used for the bug filing (e.g. DRM/Intel) or NULL if not "
                                           "applicable. "
                                           "For GitLab, this is taken from labels matching 'component: $value'. "
                                           "To be filled automatically.")
    priority = models.CharField(max_length=50, null=True, blank=True,
                                help_text="Priority of the bug. For GitLab, this is taken from labels matching "
                                          "'priority::$value'. To be filled automatically.")
    severity = models.CharField(max_length=50, null=True, blank=True,
                                help_text="Severity of the bug. For GitLab, this is taken from labels matching "
                                          "'severity::$value'. To be filled automatically.")
    features = models.CharField(max_length=500, null=True, blank=True,
                                help_text="Coma-separated list of affected features or NULL if not applicable. "
                                          "For GitLab, this is taken from labels matching 'feature: $value'. "
                                          "To be filled automatically.")
    platforms = models.CharField(max_length=500, null=True, blank=True,
                                 help_text="Coma-separated list of affected platforms or NULL if not applicable. "
                                           "For GitLab, this is taken from labels matching 'platform: $value'. "
                                           "To be filled automatically.")
    status = models.CharField(max_length=100, null=True, blank=True,
                              help_text="Status of the bug (e.g. RESOLVED/FIXED). To be filled automatically.")
    tags = models.TextField(null=True, blank=True,
                            help_text="Stores a comma-separated list of Bug tags/labels. "
                                      "To be filled automatically")

    # TODO: Metric on time between creation and first assignment. Metric on time between creation and component updated

    comments_polled = models.DateTimeField(null=True, blank=True,
                                           help_text="Last time the comments of the bug were polled. "
                                                     "To be filled automatically.")

    flagged_as_update_pending_on = models.DateTimeField(null=True, blank=True,
                                                        help_text="Date at which a developer indicated their "
                                                                  "willingness to update the bug")
    custom_fields = JSONField(help_text="Mapping of customfields and values for the Bug. This field will be "
                                        "automatically populated based on BugTracker.custom_fields_map field",
                              default=dict)

    class Meta:
        unique_together = (('tracker', 'bug_id'), ('parent', 'tracker'))

    rd_only_fields = ['id', 'bug_id', 'tracker_id', 'tracker', 'parent_id', 'parent']

    @property
    def short_name(self):
        return "{}{}{}".format(self.tracker.short_name, self.tracker.separator, self.bug_id)

    @property
    def url(self):
        return "{}{}".format(self.tracker.bug_base_url, self.bug_id)

    @property
    def features_list(self):
        if self.features is not None and len(self.features) > 0:
            return [f.strip() for f in self.features.split(',')]
        else:
            return []

    @property
    def platforms_list(self):
        if self.platforms is not None and len(self.platforms) > 0:
            return [p.strip() for p in self.platforms.split(',')]
        else:
            return []

    @property
    def tags_list(self):
        if self.tags is not None and len(self.tags) > 0:
            return [t.strip() for t in self.tags.split(',')]
        else:
            return []

    @property
    def is_open(self):
        return self.tracker.is_bug_open(self)

    @property
    def has_new_comments(self):
        return self.comments_polled is None or self.comments_polled < self.updated

    @cached_property
    def comments_cached(self):
        return BugComment.objects.filter(bug=self).prefetch_related("account", "account__person")

    @cached_property
    def involves(self):
        actors = defaultdict(lambda: 0)
        actors[self.creator] += 1  # NOTE: on bugzilla, we will double count the first post
        for comment in self.comments_cached:
            actors[comment.account] += 1

        sorted_actors = OrderedDict()
        for account in sorted(actors.keys(), key=lambda k: actors[k], reverse=True):
            sorted_actors[account] = actors[account]

        return sorted_actors

    def __last_updated_by__(self, is_dev):
        last = None
        for comment in self.comments_cached:
            # TODO: make that if a developer wrote a new bug, he/she needs to be considered as a user
            if comment.account.is_developer == is_dev and (last is None or comment.created_on > last):
                last = comment.created_on
        return last

    @cached_property
    def last_updated_by_user(self):
        return self.__last_updated_by__(False)

    @cached_property
    def last_updated_by_developer(self):
        return self.__last_updated_by__(True)

    @cached_property
    def SLA(self):
        if self.priority is not None:
            return self.tracker.SLAs_cached.get(self.priority.lower(), timedelta.max)
        else:
            return timedelta.max

    @cached_property
    def SLA_deadline(self):
        if self.last_updated_by_developer is not None:
            # We have a comment, follow the SLA of the bug
            if self.SLA != timedelta.max:
                return self.last_updated_by_developer + self.SLA
            else:
                return timezone.now() + timedelta(days=365, seconds=1)
        else:
            # We have not done the initial triaging, give some time for the initial response
            return self.created + self.tracker.first_response_SLA

    @cached_property
    def SLA_remaining_time(self):
        diff = self.SLA_deadline - timezone.now()
        return timedelta(seconds=int(diff.total_seconds()))

    @cached_property
    def SLA_remaining_str(self):
        rt = self.SLA_remaining_time
        if rt < timedelta(0):
            return str(rt)[1:] + " ago"
        else:
            return "in " + str(rt)

    @cached_property
    def effective_priority(self):
        return -self.SLA_remaining_time / self.SLA

    @property
    def is_being_updated(self):
        if self.flagged_as_update_pending_on is None:
            return False
        else:
            return timezone.now() - self.flagged_as_update_pending_on < self.UPDATE_PENDING_TIMEOUT

    @property
    def update_pending_expires_in(self):
        if self.flagged_as_update_pending_on is None:
            return None
        return (self.flagged_as_update_pending_on + self.UPDATE_PENDING_TIMEOUT) - timezone.now()

    def clean(self):
        if self.custom_fields is None:
            return

        for field, value in self.custom_fields.items():
            if isinstance(value, dict) or isinstance(value, list) or isinstance(value, tuple):
                raise ValueError('Values stored in custom_fields cannot be tuples, lists, dictionaries')

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    def update_from_dict(self, upd_dict):
        if not upd_dict:
            return

        for field in upd_dict:
            # Disallow updating some critical fields
            if field in Bug.rd_only_fields:
                continue

            if hasattr(self, field):
                setattr(self, field, upd_dict[field])

    def poll(self, force_polling_comments=False):
        self.tracker.poll(self, force_polling_comments)

    def add_comment(self, comment):
        self.tracker.tracker.add_comment(self, comment)

    def create(self):
        try:
            id = self.tracker.tracker.create_bug(self)
        except ValueError:         # pragma: no cover
            traceback.print_exc()  # pragma: no cover
        else:
            self.bug_id = id

    def __str__(self):
        return "{} - {}".format(self.short_name, self.title)


class BugComment(models.Model, UserFiltrableMixin):
    filter_objects_to_db = {
        'filter_description':
            FilterObjectStr('bug__issue__filters__description',
                            'Description of what a filter associated to an issue referencing the bug on which the comment was made matches'),  # noqa
        'filter_runconfig_tag_name':
            FilterObjectStr('bug__issue__filters__tags__name',
                            'Run configuration tag matched by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_machine_tag_name':
            FilterObjectStr('bug__issue__filters__machine_tags__name',
                            'Machine tag matched by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_machine_name':
            FilterObjectStr('bug__issue__filters__machines__name',
                            'Name of a machine matched by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_test_name':
            FilterObjectStr('bug__issue__filters__tests__name',
                            'Name of a test matched by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_status_name':
            FilterObjectStr('bug__issue__filters__statuses__name',
                            'Status matched by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_stdout_regex':
            FilterObjectStr('bug__issue__filters__stdout_regex',
                            'Standard output regex used by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_stderr_regex':
            FilterObjectStr('bug__issue__filters__stderr_regex',
                            'Standard error regex used by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_dmesg_regex':
            FilterObjectStr('bug__issue__filters__dmesg_regex',
                            'Regex for dmesg used by the filter associated to an issue referencing the bug on which the comment was made'),  # noqa
        'filter_added_on':
            FilterObjectDateTime('bug__issue__issuefilterassociated__added_on',
                                 'Date at which the filter was associated to the issue referencing the bug on which the comment was made'),  # noqa
        'filter_covers_from':
            FilterObjectDateTime('bug__issue__issuefilterassociated__covers_from',
                                 'Date of the first failure covered by the filter associated to the issue referencing the bug on which the comment was made'),  # noqa
        'filter_deleted_on':
            FilterObjectDateTime('bug__issue__issuefilterassociated__deleted_on',
                                 'Date at which the filter was removed from the issue referencing the bug on which the comment was made'),  # noqa
        'filter_runconfigs_covered_count':
            FilterObjectInteger('bug__issue__issuefilterassociated__runconfigs_covered_count',
                                'Amount of run configurations covered by the filter associated to the issue referencing the bug on which the comment was made'),  # noqa
        'filter_runconfigs_affected_count':
            FilterObjectInteger('bug__issue__issuefilterassociated__runconfigs_affected_count',
                                'Amount of run configurations affected by the filter associated to the issue referencing the bug on which the comment was made'),  # noqa
        'filter_last_seen':
            FilterObjectDateTime('bug__issue__issuefilterassociated__last_seen',
                                 'Date at which the filter associated to the issue referencing the bug on which the comment was made last matched'),  # noqa
        'filter_last_seen_runconfig_name':
            FilterObjectStr('bug__issue__issuefilterassociated__last_seen_runconfig__name',
                            'Run configuration which last matched the filter associated to the issue referencing the bug on which the comment was made'),  # noqa

        'issue_description': FilterObjectStr('bug__issue__description',
                                             'Free-hand text associated to the issue by the bug filer'),
        'issue_filer_email': FilterObjectStr('bug__issue__filer', 'Email address of the person who filed the issue'),
        'issue_added_on': FilterObjectDateTime('bug__issue__added_on', 'Date at which the issue was created'),
        'issue_archived_on': FilterObjectDateTime('bug__issue__archived_on', 'Date at which the issue was archived'),
        'issue_expected': FilterObjectBool('bug__issue__expected', 'Is the issue expected?'),
        'issue_runconfigs_covered_count': FilterObjectInteger('bug__issue__runconfigs_covered_count',
                                                              'Amount of run configurations covered by the issue'),
        'issue_runconfigs_affected_count': FilterObjectInteger('bug__issue__runconfigs_affected_count',
                                                               'Amount of run configurations affected by the issue'),
        'issue_last_seen': FilterObjectDateTime('bug__issue__last_seen', 'Date at which the issue was last seen'),
        'issue_last_seen_runconfig_name': FilterObjectStr('bug__issue__last_seen_runconfig__name',
                                                          'Run configuration which last reproduced the issue'),

        'tracker_name': FilterObjectStr('bug__tracker__name', 'Name of the tracker hosting the bug'),
        'tracker_short_name': FilterObjectStr('bug__tracker__short_name', 'Short name of the tracker which hosts the bug'),  # noqa
        'tracker_type': FilterObjectStr('bug__tracker__tracker_type', 'Type of the tracker which hosts the bug'),
        'bug_id': FilterObjectStr('bug__bug_id', 'ID of the bug on which the comment was made'),
        'bug_title': FilterObjectStr('bug__title', 'Title of the bug on which the comment was made'),
        'bug_created_on': FilterObjectDateTime('bug__created',
                                               'Date at which the bug on which the comment was made was created'),
        'bug_updated_on': FilterObjectDateTime('bug__updated',
                                               'Date at which the bug on which the comment was made was last updated'),
        'bug_closed_on': FilterObjectDateTime('bug__closed',
                                              'Date at which the bug on which the comment was made was closed'),
        'bug_creator_name': FilterObjectStr('bug__creator__person__full_name',
                                            'Name of the creator of the bug on which the comment was made'),
        'bug_creator_email': FilterObjectStr('bug__creator__person__email',
                                             'Email address of the creator of the bug on which the comment was made'),
        'bug_assignee_name': FilterObjectStr('bug__assignee__person__full_name',
                                             'Name of the assignee of the bug on which the comment was made'),
        'bug_assignee_email': FilterObjectStr('bug__assignee__person__email',
                                              'Email address of the assignee of the bug on which the comment was made'),
        'bug_product': FilterObjectStr('bug__product', 'Product of the bug on which the comment was made'),
        'bug_component': FilterObjectStr('bug__component', 'Component of the bug on which the comment was made'),
        'bug_priority': FilterObjectStr('bug__priority', 'Priority of the bug on which the comment was made'),
        'bug_features': FilterObjectStr('bug__features',
                                        'Features affected (coma-separated list) in the bug on which the comment was made'),  # noqa
        'bug_platforms': FilterObjectStr('bug__platforms',
                                         'Platforms affected (coma-separated list) in the bug on which the comment was made'),  # noqa
        'bug_status': FilterObjectStr('bug__status',
                                      'Status of the bug (RESOLVED/FIXED, ...) on which the comment was made'),
        'bug_tags': FilterObjectStr('bug__tags', 'Tags/labels associated to the bug (coma-separated list)'),

        'creator_name': FilterObjectStr('account__person__full_name', 'Name of the creator of the comment'),
        'creator_email': FilterObjectStr('account__person__email', 'Email address of the creator of the comment'),
        'creator_is_developer': FilterObjectBool('account__is_developer', 'Is the creator of the comment a developer?'),
        'comment_id': FilterObjectInteger('comment_id', 'The ID of the comment'),
        'created_on': FilterObjectDateTime('created_on', 'Date at wich the comment was made')
    }

    bug = models.ForeignKey(Bug, on_delete=models.CASCADE)
    account = models.ForeignKey(BugTrackerAccount, on_delete=models.CASCADE)

    comment_id = models.CharField(max_length=20, help_text="The ID of the comment")
    url = models.URLField(null=True, blank=True)
    created_on = models.DateTimeField()

    class Meta:
        unique_together = ('bug', 'comment_id')

    def __str__(self):
        return "{}'s comment by {}".format(self.bug, self.account)


def script_validator(script):
    try:
        client = Client.get_or_create_instance(script)
    except ValueError as e:
        raise ValidationError("Script contains syntax errors: {}".format(e))
    else:
        client.shutdown()
        return script


class ReplicationScript(models.Model):
    """These scripts provide a method for replicating bugs between different bugtrackers, based
    on the user defined Python script. Further documentation on the process and API can be found
    here - :ref:`replication-doc`
    """
    name = models.CharField(max_length=50, unique=True, help_text="Unique name for the script")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, help_text="The author or last editor of the script",
                                   related_name='script_creator', null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True,
                                      help_text="Date the script was created or last updated")
    enabled = models.BooleanField(default=False, help_text="Enable bug replication")
    source_tracker = models.ForeignKey(BugTracker, related_name="source_rep_script", on_delete=models.CASCADE,
                                       null=True, help_text="Tracker to replicate from")
    destination_tracker = models.ForeignKey(BugTracker, related_name="dest_rep_script", on_delete=models.CASCADE,
                                            null=True, help_text="Tracker to replicate to")
    script = models.TextField(null=True, blank=True,
                              help_text="Python script to be executed", validators=[script_validator])
    script_history = models.TextField(default='[]',
                                      help_text="Stores the script edit history of the ReplicationScript model "
                                      "in JSON format. The keys correspond to all the fields in the ReplicationScript "
                                      "model, excluding this 'script_history' field itself.")

    class Meta:
        unique_together = ('source_tracker', 'destination_tracker')

    def __str__(self):
        return "<replication script '{}'>".format(self.name)


# Software
class Component(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField()
    url = models.URLField(null=True, blank=True)
    public = models.BooleanField(help_text="Should the component (and its builds) be visible on the public website?")

    def __str__(self):
        return self.name


class Build(models.Model):
    # Minimum information needed
    name = models.CharField(max_length=40, unique=True)
    component = models.ForeignKey(Component, on_delete=models.CASCADE)
    version = models.CharField(max_length=40)
    added_on = models.DateTimeField(auto_now=True)

    # Allow creating an overlay over the history of the component
    parents = models.ManyToManyField('Build', blank=True)

    # Actual build information
    repo_type = models.CharField(max_length=50, null=True, blank=True)
    branch = models.CharField(max_length=50, null=True, blank=True)
    repo = models.URLField(null=True, blank=True)
    upstream_url = models.URLField(null=True, blank=True)
    parameters = models.TextField(null=True, blank=True)
    build_log = models.TextField(null=True, blank=True)

    @property
    def url(self):
        if self.upstream_url is not None:
            return self.upstream_url
        elif self.repo is not None:
            return "{} @ {}".format(self.version, self.repo)
        else:
            return self.version

    def __str__(self):
        return self.name

# Results


class VettableObjectMixin:
    @property
    def vetted(self):
        return self.vetted_on is not None

    @transaction.atomic
    def vet(self):
        if self.vetted_on is not None:
            raise ValueError('The object is already vetted')
        self.vetted_on = timezone.now()
        self.save()

    @transaction.atomic
    def suppress(self):
        if self.vetted_on is None:
            raise ValueError('The object is already suppressed')
        self.vetted_on = None
        self.save()


class Test(VettableObjectMixin, models.Model):
    name = models.CharField(max_length=150)
    testsuite = models.ForeignKey('TestSuite', on_delete=models.CASCADE)
    public = models.BooleanField(db_index=True, help_text="Should the test be visible on the public website?")
    vetted_on = models.DateTimeField(db_index=True, null=True, blank=True,
                                     help_text="When did the test get ready for pre-merge testing?")
    added_on = models.DateTimeField(auto_now_add=True)
    first_runconfig = models.ForeignKey('RunConfig', db_index=True, null=True, on_delete=models.SET_NULL,
                                        help_text="First non-temporary runconfig that executed this test")

    class Meta:
        ordering = ['name']
        unique_together = ('name', 'testsuite')
        permissions = [
            ("vet_test", "Can vet a test"),
            ("suppress_test", "Can suppress a test"),
        ]

    def __str__(self):
        return "{}: {}".format(self.testsuite, self.name)

    @property
    def in_active_ifas(self):
        return IssueFilterAssociated.objects.filter(deleted_on=None, filter__tests__in=[self])

    @transaction.atomic
    def rename(self, new_name):
        # Get the matching test, or create it
        new_test = Test.objects.filter(name=new_name, testsuite=self.testsuite).first()
        if new_test is None:
            new_test = Test.objects.create(name=new_name, testsuite=self.testsuite,
                                           public=self.public)
        else:
            new_test.public = self.public

        new_test.vetted_on = self.vetted_on
        new_test.save()

        # Now, update every active IFA
        for ifa in self.in_active_ifas:
            ifa.filter.tests.add(new_test)


class MachineTag(models.Model):
    name = models.CharField(max_length=30, unique=True)
    description = models.TextField(help_text="Description of the objectives of the tag", blank=True, null=True)
    public = models.BooleanField(db_index=True, help_text="Should the machine tag be visible on the public website?")

    added_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['name']

    @cached_property
    def machines(self):
        return sorted(Machine.objects.filter(tags__in=[self]), key=lambda m: m.name)

    def __str__(self):
        return self.name


class Machine(VettableObjectMixin, ColoredObjectMixin, models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(help_text="Description of the machine", blank=True, null=True)

    public = models.BooleanField(db_index=True, help_text="Should the machine be visible on the public website?")

    vetted_on = models.DateTimeField(db_index=True, null=True, blank=True,
                                     help_text="When did the machine get ready for pre-merge testing?")

    added_on = models.DateTimeField(auto_now_add=True)

    aliases = models.ForeignKey("Machine", on_delete=models.CASCADE, null=True, blank=True,
                                help_text="This machine is an alias of another machine. "
                                "The aliased machine will be used when comparing runconfigs. "
                                "This is useful if you have multiple identical machines that "
                                "execute a different subset of test every run")

    tags = models.ManyToManyField(MachineTag, blank=True)

    color_hex = ColoredObjectMixin.color_hex

    class Meta:
        ordering = ['name']
        permissions = [
            ("vet_machine", "Can vet a machine"),
            ("suppress_machine", "Can suppress a machine"),
        ]

    @cached_property
    def tags_cached(self):
        return self.tags.all()

    def __str__(self):
        return self.name


class RunConfigTag(models.Model):
    name = models.CharField(max_length=50, unique=True,
                            help_text="Unique name for the tag")
    description = models.TextField(help_text="Description of the objectives of the tag")
    url = models.URLField(null=True, blank=True, help_text="URL to more explanations (optional)")
    public = models.BooleanField(help_text="Should the tag be visible on the public website?")

    def __str__(self):
        return self.name


class RunConfig(models.Model):
    filter_objects_to_db = {
        'name': FilterObjectStr('name', 'Name of the run configuration'),
        'tag': FilterObjectStr('tags__name', 'Tag associated with the configuration for this run'),
        'added_on': FilterObjectDateTime('added_on', 'Date at which the run configuration got created'),
        'temporary': FilterObjectBool('temporary', 'Is the run configuration temporary (pre-merge testing)?'),
        'build': FilterObjectStr('builds__name', 'Tag associated with the configuration for this run'),
        'environment': FilterObjectStr('environment', 'Free-text field describing the environment of the machine'),

        # Through reverse accessors
        'machine_name': FilterObjectStr('testsuiterun__machine__name', 'Name of the machine used in this run'),
        'machine_tag': FilterObjectStr('testsuiterun__machine__tags__name',
                                       'Tag associated to the machine used in this run'),
    }

    name = models.CharField(max_length=50, unique=True)
    tags = models.ManyToManyField(RunConfigTag)
    temporary = models.BooleanField(help_text="This runconfig is temporary and should not be part of statistics")
    url = models.URLField(null=True, blank=True)

    added_on = models.DateTimeField(auto_now_add=True, db_index=True)

    builds = models.ManyToManyField(Build)

    environment = models.TextField(null=True, blank=True,
                                   help_text="A human-readable, and machine-parsable definition of the environment. "
                                             "Make sure the environment contains a header with the format and version.")

    @cached_property
    def tags_cached(self):
        return self.tags.all()

    @cached_property
    def tags_ids_cached(self):
        return set([t.id for t in self.tags_cached])

    @cached_property
    def builds_cached(self):
        return self.builds.all()

    @cached_property
    def builds_ids_cached(self):
        return set([b.id for b in self.builds_cached])

    @cached_property
    def public(self):
        for tag in self.tags_cached:
            if not tag.public:
                return False
        return True

    @cached_property
    def runcfg_history(self):
        # TODO: we may want to use something else but the tags to find out
        #       the history of this particular run config

        # TODO 2: make sure the tags sets are equal, not just that a set is inside
        # another one. This is a regression caused by django 2.0
        tags = self.tags_cached
        return RunConfig.objects.order_by("-added_on").filter(tags__in=tags, temporary=False)

    @cached_property
    def runcfg_history_offset(self):
        for i, runcfg in enumerate(self.runcfg_history):
            if self.id == runcfg.id:
                return i
        raise ValueError("BUG: The runconfig ID has not been found in the runconfig history")

    def __str__(self):
        return self.name

    def update_statistics(self, results=None, ifas=None):
        stats = []

        # Do not compute statistics for temporary runconfigs
        if self.temporary:
            return stats

        # Fetch the list of results, pre-fetching the fields necessary to
        # compute if the result is a failure or not
        if results is None:
            results = TestResult.objects.prefetch_related('status__testsuite__acceptable_statuses',
                                                          'status', 'ts_run__machine',
                                                          'ts_run__machine__tags',
                                                          'ts_run__runconfig__tags',
                                                          'test').filter(ts_run__runconfig=self)

        # Fetch the list of IFAs if has not been specified
        if ifas is None:
            ifas = IssueFilterAssociated.objects_ready_for_matching.filter(Q(deleted_on=None))

        # Check if all filters cover and/or match results. De-dupplicate filters first
        filters = set([e.filter for e in ifas])
        for filter in filters:
            fs = RunFilterStatistic(filter=filter, runconfig=self, covered_count=0,
                                    matched_count=0)

            for result in results:
                if filter.covers(result):
                    fs.covered_count += 1

                    if result.is_failure and filter.matches(result, skip_cover_test=True):
                        fs.matched_count += 1

            if fs.covered_count > 0:
                stats.append(fs)

        # Remove all the stats for the current run, and add the new ones
        with transaction.atomic():
            RunFilterStatistic.objects.filter(runconfig=self, filter__in=filters).delete()
            RunFilterStatistic.objects.bulk_create(stats)

        return stats

    def compare(self, to, max_missing_hosts=0.5, no_compress=False):
        return RunConfigDiff(self, to, max_missing_hosts=max_missing_hosts, no_compress=no_compress)


class TestSuite(VettableObjectMixin, Component):
    vetted_on = models.DateTimeField(db_index=True, null=True, blank=True,
                                     help_text="When did the testsuite get ready for pre-merge testing?")

    # List of results you do not want to file bugs for
    acceptable_statuses = models.ManyToManyField('TextStatus', related_name='+', blank=True)

    # Status to ignore for diffing
    notrun_status = models.ForeignKey('TextStatus', null=True, blank=True, on_delete=models.SET_NULL,
                                      related_name='+')

    class Meta:
        permissions = [
            ("vet_testsuite", "Can vet a testsuite"),
            ("suppress_testsuite", "Can suppress a testsuite"),
        ]

    @cached_property
    def __acceptable_statuses__(self):
        return set([r.id for r in self.acceptable_statuses.all()])

    def __str__(self):
        return self.name

    def is_failure(self, status):
        return status.id not in self.__acceptable_statuses__


class TestsuiteRun(models.Model, UserFiltrableMixin):
    # For the FilterMixin.
    filter_objects_to_db = {
            'testsuite_name': FilterObjectStr('testsuite__name',
                                              'Name of the testsuite that was used for this run'),
            'runconfig': FilterObjectModel(RunConfig, 'runconfig', 'Run configuration the test is part of'),
            'runconfig_name': FilterObjectStr('runconfig__name', 'Name of the run configuration'),
            'runconfig_tag': FilterObjectStr('runconfig__tags__name',
                                             'Tag associated with the configuration for this run'),
            'runconfig_added_on': FilterObjectDateTime('runconfig__added_on',
                                                       'Date at which the run configuration got created'),
            'runconfig_temporary': FilterObjectBool('runconfig__temporary',
                                                    'Is the run configuration temporary (pre-merge testing)?'),
            'machine_name': FilterObjectStr('machine__name', 'Name of the machine used in this run'),
            'machine_tag': FilterObjectStr('machine__tags__name', 'Tag associated to the machine used in this run'),
            'url': FilterObjectStr('url', 'External URL associated to this testsuite run'),
            'start': FilterObjectDateTime('start', "Local time at witch the run started on the machine"),
            'duration': FilterObjectDuration('duration', 'Duration of the testsuite run'),
            'reported_on': FilterObjectDateTime('reported_on',
                                                'Date at which the testsuite run got imported in CI Bug Log'),
            'environment': FilterObjectStr('environment', 'Free-text field describing the environment of the machine'),
            'log': FilterObjectStr('log', 'Log of the testsuite run'),
        }

    testsuite = models.ForeignKey(TestSuite, on_delete=models.CASCADE)
    runconfig = models.ForeignKey(RunConfig, on_delete=models.CASCADE)
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE)
    run_id = models.IntegerField()
    url = models.URLField(null=True, blank=True)

    start = models.DateTimeField()
    duration = models.DurationField()
    reported_on = models.DateTimeField(auto_now_add=True)

    environment = models.TextField(blank=True,
                                   help_text="A human-readable, and machine-parsable definition of the environment. "
                                             "Make sure the environment contains a header with the format and version.")
    log = models.TextField(blank=True)

    class Meta:
        unique_together = ('testsuite', 'runconfig', 'machine', 'run_id')
        ordering = ['start']

    def __str__(self):
        return "{} on {} - testsuite run {}".format(self.runconfig.name, self.machine.name, self.run_id)


class TextStatus(VettableObjectMixin, ColoredObjectMixin, models.Model):
    testsuite = models.ForeignKey(TestSuite, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)

    vetted_on = models.DateTimeField(db_index=True, null=True, blank=True,
                                     help_text="When did the status get ready for pre-merge testing?")

    color_hex = ColoredObjectMixin.color_hex

    severity = models.PositiveIntegerField(null=True, blank=True,
                                           help_text="Define how bad a the status is, from better to worse. "
                                                     "The best possible is 0.")

    class Meta:
        unique_together = ('testsuite', 'name')
        verbose_name_plural = "Text Statuses"
        permissions = [
            ("vet_textstatus", "Can vet a text status"),
            ("suppress_textstatus", "Can suppress a text status"),
        ]

    @property
    def is_failure(self):
        return self.testsuite.is_failure(self)

    @property
    def is_notrun(self):
        return self == self.testsuite.notrun_status

    @property
    def actual_severity(self):
        if self.severity is not None:
            return self.severity
        elif self.is_notrun:
            return 0
        elif not self.is_failure:
            return 1
        else:
            return 2

    def __str__(self):
        return "{}: {}".format(self.testsuite, self.name)


class TestResultAssociatedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('status__testsuite__acceptable_statuses',
                                                       'status', 'ts_run__machine',
                                                       'ts_run__machine__tags',
                                                       'ts_run__runconfig__tags',
                                                       'test')


class TestResult(models.Model, UserFiltrableMixin):
    # For the FilterMixin.
    filter_objects_to_db = {
        'runconfig': FilterObjectModel(RunConfig, 'ts_run__runconfig', 'Run configuration the test is part of'),
        'runconfig_name': FilterObjectStr('ts_run__runconfig__name', 'Name of the run configuration'),
        'runconfig_tag': FilterObjectStr('ts_run__runconfig__tags__name',
                                         'Tag associated with the configuration used for this test execution'),
        'runconfig_added_on': FilterObjectDateTime('ts_run__runconfig__added_on',
                                                   'Date at which the run configuration got created'),
        'runconfig_temporary': FilterObjectBool('ts_run__runconfig__temporary',
                                                'Is the run configuration temporary, like for pre-merge testing?'),
        'build_name': FilterObjectStr('ts_run__runconfig__builds__name',
                                      'Name of the build for a component used for this test execution'),
        'component_name': FilterObjectStr('ts_run__runconfig__builds__component__name',
                                          'Name of a component used for this test execution'),
        'machine_name': FilterObjectStr('ts_run__machine__name', 'Name of the machine used for this result'),
        'machine_tag': FilterObjectStr('ts_run__machine__tags__name',
                                       'Tag associated to the machine used in this run'),
        'status_name': FilterObjectStr('status__name', 'Name of the resulting status (pass/fail/crash/...)'),
        'testsuite_name': FilterObjectStr('status__testsuite__name',
                                          'Name of the testsuite that contains this test'),
        'test_name': FilterObjectStr('test__name', 'Name of the test'),
        'test_added_on': FilterObjectDateTime('test__added_on', 'Date at which the test got added'),
        'manually_filed_on': FilterObjectDateTime('known_failure__manually_associated_on',
                                                  'Date at which the failure got manually associated to an issue'),
        'ifa_id': FilterObjectInteger('known_failure__matched_ifa_id',
                                      'ID of the associated filter that matched the failure'),
        'issue_id': FilterObjectInteger('known_failure__matched_ifa__issue_id',
                                        'ID of the issue associated to the failure'),
        'issue_expected': FilterObjectBool('known_failure__matched_ifa__issue__expected',
                                           'Is the issue associated to the failure marked as expected?'),
        'filter_description': FilterObjectStr('known_failure__matched_ifa__issue__filters__description',
                                              'Description of what the filter associated to the failure'),
        'filter_runconfig_tag_name':
            FilterObjectStr('known_failure__matched_ifa__issue__filters__tags__name',
                            'Run configuration tag matched by the filter associated to the failure'),
        'filter_machine_tag_name': FilterObjectStr('known_failure__matched_ifa__issue__filters__machine_tags__name',
                                                   'Machine tag matched by the filter associated to the failure'),
        'filter_machine_name': FilterObjectStr('known_failure__matched_ifa__issue__filters__machines__name',
                                               'Name of a machine matched by the filter associated to the failure'),
        'filter_test_name': FilterObjectStr('known_failure__matched_ifa__issue__filters__tests__name',
                                            'Name of a test matched by the filter associated to the failure'),
        'filter_status_name': FilterObjectStr('known_failure__matched_ifa__issue__filters__statuses__name',
                                              'Status matched by the filter associated to the failure'),
        'filter_stdout_regex': FilterObjectStr('known_failure__matched_ifa__issue__filters__stdout_regex',
                                               'Standard output regex used by the filter associated to the failure'),
        'filter_stderr_regex': FilterObjectStr('known_failure__matched_ifa__issue__filters__stderr_regex',
                                               'Standard error regex used by the filter associated to the failure'),
        'filter_dmesg_regex': FilterObjectStr('known_failure__matched_ifa__issue__filters__dmesg_regex',
                                              'Regex for dmesg used by the filter associated to the failure'),
        'filter_added_on': FilterObjectDateTime('known_failure__matched_ifa__issue__issuefilterassociated__added_on',
                                                'Date at which the filter associated to the failure was added on to its issue'),  # noqa
        'filter_covers_from':
            FilterObjectDateTime('known_failure__matched_ifa__issue__issuefilterassociated__covers_from',
                                 'Date of the first failure covered by the filter associated to the failure'),
        'filter_deleted_on':
            FilterObjectDateTime('known_failure__matched_ifa__issue__issuefilterassociated__deleted_on',
                                 'Date at which the filter was removed from the issue associated to the failure'),
        'filter_runconfigs_covered_count':
            FilterObjectInteger('known_failure__matched_ifa__issue__issuefilterassociated__runconfigs_covered_count',
                                'Amount of run configurations covered by the filter associated to the failure'),
        'filter_runconfigs_affected_count':
            FilterObjectInteger('known_failure__matched_ifa__issue__issuefilterassociated__runconfigs_affected_count',
                                'Amount of run configurations affected by the filter associated to the failure'),
        'filter_last_seen':
            FilterObjectDateTime('known_failure__matched_ifa__issue__issuefilterassociated__last_seen',
                                 'Date at which the filter matching this failure was last seen'),
        'filter_last_seen_runconfig_name':
            FilterObjectStr('known_failure__matched_ifa__issue__issuefilterassociated__last_seen_runconfig__name',
                            'Run configuration which last matched the filter associated to the failure'),
        'bug_tracker_name': FilterObjectStr('known_failure__matched_ifa__issue__bugs__tracker__name',
                                            'Name of the tracker which holds the bug associated to this failure'),
        'bug_tracker_short_name': FilterObjectStr('known_failure__matched_ifa__issue__bugs__tracker__short_name',
                                                  'Short name of the tracker which holds the bug associated to this failure'),  # noqa
        'bug_tracker_type': FilterObjectStr('known_failure__matched_ifa__issue__bugs__tracker__tracker_type',
                                            'Type of the tracker which holds the bug associated to this failure'),
        'bug_id': FilterObjectStr('known_failure__matched_ifa__issue__bugs__bug_id',
                                  'ID of the bug associated to this failure'),
        'bug_title': FilterObjectStr('known_failure__matched_ifa__issue__bugs__title',
                                     'Title of the bug associated to this failure'),
        'bug_created_on': FilterObjectDateTime('known_failure__matched_ifa__issue__bugs__created',
                                               'Date at which the bug associated to this failure was created'),
        'bug_closed_on': FilterObjectDateTime('known_failure__matched_ifa__issue__bugs__closed',
                                              'Date at which the bug associated to this failure was closed'),
        'bug_creator_name': FilterObjectStr('known_failure__matched_ifa__issue__bugs__creator__person__full_name',
                                            'Name of the creator of the bug associated to this failure'),
        'bug_creator_email': FilterObjectStr('known_failure__matched_ifa__issue__bugs__creator__person__email',
                                             'Email address of the creator of the bug associated to this failure'),
        'bug_assignee_name': FilterObjectStr('known_failure__matched_ifa__issue__bugs__assignee__person__full_name',
                                             'Name of the assignee of the bug associated to this failure'),
        'bug_assignee_email': FilterObjectStr('known_failure__matched_ifa__issue__bugs__assignee__person__email',
                                              'Email address of the assignee of the bug associated to this failure'),
        'bug_product': FilterObjectStr('known_failure__matched_ifa__issue__bugs__product',
                                       'Product of the bug associated to this failure'),
        'bug_component': FilterObjectStr('known_failure__matched_ifa__issue__bugs__component',
                                         'Component of the bug associated to this failure'),
        'bug_priority': FilterObjectStr('known_failure__matched_ifa__issue__bugs__priority',
                                        'Priority of the bug associated to this failure'),
        'bug_features': FilterObjectStr('known_failure__matched_ifa__issue__bugs__features',
                                        'Features of the bug associated to this failure (coma-separated list)'),
        'bug_platforms': FilterObjectStr('known_failure__matched_ifa__issue__bugs__platforms',
                                         'Platforms of the bug associated to this failure (coma-separated list)'),
        'bug_status': FilterObjectStr('known_failure__matched_ifa__issue__bugs__status',
                                      'Status of the bug associated to this failure'),
        'bug_tags': FilterObjectStr('known_failure__matched_ifa__issue__bugs__tags',
                                    'Tags/labels on the bug associated to this failure (coma-separated list)'),
        'url': FilterObjectStr('url', 'External URL of this test result'),
        'start': FilterObjectDateTime('start', 'Date at which this test started being executed'),
        'duration': FilterObjectDuration('duration', 'Time it took to execute the test'),
        'command': FilterObjectStr('command', 'Command used to execute the test'),
        'stdout': FilterObjectStr('stdout', 'Standard output of the test execution'),
        'stderr': FilterObjectStr('stderr', 'Error output of the test execution'),
        'dmesg': FilterObjectStr('dmesg', 'Kernel logs of the test execution'),
    }

    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    ts_run = models.ForeignKey(TestsuiteRun, on_delete=models.CASCADE)
    status = models.ForeignKey(TextStatus, on_delete=models.CASCADE)

    url = models.URLField(null=True, blank=True)

    start = models.DateTimeField()
    duration = models.DurationField()

    command = models.CharField(max_length=500)
    stdout = models.TextField(null=True)
    stderr = models.TextField(null=True)
    dmesg = models.TextField(null=True)

    objects = models.Manager()
    objects_ready_for_matching = TestResultAssociatedManager()

    @cached_property
    def is_failure(self):
        return self.status.testsuite.is_failure(self.status)

    @cached_property
    def known_failures_cached(self):
        return self.known_failures.all()

    def __str__(self):
        return "{} on {} - {}: ({})".format(self.ts_run.runconfig.name, self.ts_run.machine.name,
                                            self.test.name, self.status)

# TODO: Support benchmarks too by creating BenchmarkResult (test, run, environment, ...)

# Issues


class IssueFilter(models.Model):
    description = models.CharField(max_length=255,
                                   help_text="Short description of what the filter matches!")

    tags = models.ManyToManyField(RunConfigTag, blank=True,
                                  help_text="The result's run should have at least one of these tags "
                                            "(leave empty to ignore tags)")
    machine_tags = models.ManyToManyField(MachineTag, blank=True,
                                          help_text="The result's machine should have one of these tags "
                                                    "(leave empty to ignore machines)")
    machines = models.ManyToManyField(Machine, blank=True,
                                      help_text="The result's machine should be one of these machines "
                                                "(extends the set of machines selected by the machine tags, "
                                                "leave empty to ignore machines)")
    tests = models.ManyToManyField(Test, blank=True,
                                   help_text="The result's machine should be one of these tests "
                                             "(leave empty to ignore tests)")
    statuses = models.ManyToManyField(TextStatus, blank=True,
                                      help_text="The result's status should be one of these (leave empty to "
                                                "ignore results)")

    stdout_regex = models.CharField(max_length=1000, blank=True,
                                    help_text="The result's stdout field must contain a substring matching this "
                                              "regular expression (leave empty to ignore stdout)")
    stderr_regex = models.CharField(max_length=1000, blank=True,
                                    help_text="The result's stderr field must contain a substring matching this "
                                              "regular expression (leave empty to ignore stderr)")
    dmesg_regex = models.CharField(max_length=1000, blank=True,
                                   help_text="The result's dmesg field must contain a substring matching this "
                                             "regular expression (leave empty to ignore dmesg)")

    added_on = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False, db_index=True, help_text="Do not show this filter in filter lists")

    def delete(self):
        self.hidden = True

    @cached_property
    def tags_cached(self):
        return set(self.tags.all())

    @cached_property
    def tags_ids_cached(self):
        return set([t.id for t in self.tags_cached])

    @cached_property
    def __machines_cached__(self):
        return set(self.machines.all())

    @cached_property
    def __machine_tags_cached__(self):
        return set(self.machine_tags.all())

    @cached_property
    def machines_cached(self):
        machines = self.__machines_cached__
        for machine in Machine.objects.filter(tags__in=self.__machine_tags_cached__):
            machines.add(machine)
        return machines

    @cached_property
    def machines_ids_cached(self):
        return set([m.id for m in self.machines_cached])

    @cached_property
    def tests_cached(self):
        return set(self.tests.all())

    @cached_property
    def tests_ids_cached(self):
        return set([m.id for m in self.tests_cached])

    @cached_property
    def statuses_cached(self):
        return set(self.statuses.all().select_related('testsuite'))

    @cached_property
    def statuses_ids_cached(self):
        return set([s.id for s in self.statuses_cached])

    @cached_property
    def stdout_regex_cached(self):
        return re.compile(self.stdout_regex)

    @cached_property
    def stderr_regex_cached(self):
        return re.compile(self.stderr_regex)

    @cached_property
    def dmesg_regex_cached(self):
        return re.compile(self.dmesg_regex)

    @property
    def covered_results(self):
        return TestResult.from_user_filters(**self._to_user_query(covers=True, matches=False)).objects

    def covers(self, result):
        # WARNING: Make sure to update the _to_user_query() method when changing
        # the definition of "covers"
        if len(self.tags_ids_cached) > 0:
            if result.ts_run.runconfig.tags_ids_cached.isdisjoint(self.tags_ids_cached):
                return False

        if len(self.machines_ids_cached) > 0 or len(self.__machine_tags_cached__) > 0:
            if result.ts_run.machine_id not in self.machines_ids_cached:
                return False

        if len(self.tests_ids_cached) > 0:
            if result.test_id not in self.tests_ids_cached:
                return False

        return True

    def matches(self, result, skip_cover_test=False):
        # WARNING: Make sure to update the _to_user_query() method when changing
        # the definition of "matches"
        if not skip_cover_test and not self.covers(result):
            return False

        if len(self.statuses_ids_cached) > 0:
            if result.status_id not in self.statuses_ids_cached:
                return False

        if len(self.stdout_regex) > 0:
            if self.stdout_regex_cached.search(result.stdout) is None:
                return False

        if len(self.stderr_regex) > 0:
            if self.stderr_regex_cached.search(result.stderr) is None:
                return False

        if len(self.dmesg_regex) > 0:
            if self.dmesg_regex_cached.search(result.dmesg) is None:
                return False

        return True

    @transaction.atomic
    def replace(self, new_filter, user):
        # Go through all the issues that currently use this filter
        for e in IssueFilterAssociated.objects.filter(deleted_on=None, filter=self):
            e.issue.replace_filter(self, new_filter, user)

        # Hide this filter now and only keep it for archive purposes
        self.delete()

    def _to_user_query(self, covers=True, matches=True):
        query = []

        if covers:
            if len(self.tags_cached) > 0:
                query.append('runconfig_tag IS IN ["{}"]'.format('", "'.join([t.name for t in self.tags_cached])))

            if len(self.__machines_cached__) > 0 or len(self.__machine_tags_cached__) > 0:
                if len(self.__machines_cached__) > 0:
                    machines = [m.name for m in self.__machines_cached__]
                    machines_query = 'machine_name IS IN ["{}"]'.format('", "'.join(machines))
                if len(self.__machine_tags_cached__) > 0:
                    tags = [t.name for t in self.__machine_tags_cached__]
                    machine_tags_query = 'machine_tag IS IN ["{}"]'.format('", "'.join(tags))

                if len(self.__machines_cached__) > 0 and len(self.__machine_tags_cached__) > 0:
                    query.append("({} OR {})".format(machines_query, machine_tags_query))
                elif len(self.__machines_cached__) > 0:
                    query.append(machines_query)
                else:
                    query.append(machine_tags_query)

            if len(self.tests_cached) > 0:
                tests_query = []

                # group the tests by testsuite
                testsuites = defaultdict(set)
                for test in self.tests_cached:
                    testsuites[test.testsuite].add(test)

                # create the sub-queries
                for testsuite in testsuites:
                    subquery = '(testsuite_name = "{}" AND test_name IS IN ["{}"])'
                    tests_query.append(subquery.format(testsuite.name,
                                                       '", "'.join([t.name for t in testsuites[testsuite]])))
                query.append("({})".format(" OR ".join(tests_query)))

        if matches:
            if len(self.statuses_cached) > 0:
                status_query = []

                # group the statuses by testsuite
                testsuites = defaultdict(set)
                for status in self.statuses_cached:
                    testsuites[status.testsuite].add(status)

                # create the sub-queries
                for testsuite in testsuites:
                    subquery = '(testsuite_name = "{}" AND status_name IS IN ["{}"])'
                    status_query.append(subquery.format(testsuite.name,
                                                        '", "'.join([s.name for s in testsuites[testsuite]])))
                query.append("({})".format(" OR ".join(status_query)))

            if len(self.stdout_regex) > 0:
                query.append("stdout ~= '{}'".format(self.stdout_regex))

            if len(self.stderr_regex) > 0:
                query.append("stderr ~= '{}'".format(self.stderr_regex))

            if len(self.dmesg_regex) > 0:
                query.append("dmesg ~= '{}'".format(self.dmesg_regex))

        return {"query": " AND ".join(query)}

    @cached_property
    def to_user_query(self):
        return self._to_user_query()

    def __str__(self):
        return self.description


class Rate:
    def __init__(self, type_str, affected, total):
        self._type_str = type_str
        self._affected = affected
        self._total = total

    @property
    def rate(self):
        if self._total > 0:
            return self._affected / self._total
        else:
            return 0

    def __str__(self):
        return "{} / {} {} ({:.1f}%)".format(self._affected,
                                             self._total,
                                             self._type_str,
                                             self.rate * 100.0)


class IssueFilterAssociatedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related('filter__tags',
                                                       'filter__machine_tags',
                                                       'filter__machines',
                                                       'filter__tests',
                                                       'filter__statuses',
                                                       'filter')


class IssueFilterAssociated(models.Model):
    filter = models.ForeignKey(IssueFilter, on_delete=models.CASCADE)
    issue = models.ForeignKey('Issue', on_delete=models.CASCADE)

    added_on = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='filter_creator',
                                 null=True, on_delete=models.SET(get_sentinel_user))

    # WARNING: Make sure this is set when archiving the issue
    deleted_on = models.DateTimeField(blank=True, null=True, db_index=True)
    deleted_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='filter_deleter',
                                   null=True, on_delete=models.SET(get_sentinel_user))

    # Statistics cache
    covers_from = models.DateTimeField(default=timezone.now)
    runconfigs_covered_count = models.PositiveIntegerField(default=0)
    runconfigs_affected_count = models.PositiveIntegerField(default=0)
    last_seen = models.DateTimeField(null=True, blank=True)
    last_seen_runconfig = models.ForeignKey('RunConfig', null=True, blank=True, on_delete=models.SET_NULL)

    objects = models.Manager()
    objects_ready_for_matching = IssueFilterAssociatedManager()

    @property
    def active(self):
        return self.deleted_on is None

    def delete(self, user, now=None):
        if self.deleted_on is not None:
            return

        if now is not None:
            self.deleted_on = now
        else:
            self.deleted_on = timezone.now()

        self.deleted_by = user
        self.save()

    @cached_property
    def __runfilter_stats_covered__(self):
        objs = RunFilterStatistic.objects.select_related("runconfig")

        # We want to look for all runs created after either when the filter
        # got associated, since the creation of the first runcfg that contains
        # a failure that retro-actively associated to this issue. Pick the
        # earliest of these two events.
        start_time = self.added_on
        if self.covers_from < start_time:
            start_time = self.covers_from

        if self.deleted_on is not None:
            return objs.filter(runconfig__added_on__gte=start_time,
                               runconfig__added_on__lt=self.deleted_on,
                               covered_count__gt=0,
                               filter__id=self.filter_id).order_by('-id')
        else:
            return objs.filter(runconfig__added_on__gte=start_time,
                               covered_count__gt=0,
                               filter__id=self.filter_id).order_by('-id')

    @cached_property
    def runconfigs_covered(self):
        return set([r.runconfig for r in self.__runfilter_stats_covered__])

    @cached_property
    def runconfigs_affected(self):
        return set([r.runconfig for r in self.__runfilter_stats_covered__ if r.matched_count > 0])

    @property
    def covered_results(self):
        q = self.filter.covered_results.filter(ts_run__runconfig__added_on__gte=self.covers_from)
        return q.prefetch_related('ts_run', 'ts_run__runconfig')

    def _add_missing_stats(self):
        # Find the list of runconfig we have stats for
        runconfigs_done = RunFilterStatistic.objects.filter(filter=self.filter).values_list('runconfig', flat=True)

        # Get the list of results, excluding the ones coming from runconfigs we already have
        stats = dict()
        results = self.covered_results.exclude(ts_run__runconfig__id__in=runconfigs_done).only('id', 'ts_run')
        for result in results:
            runconfig = result.ts_run.runconfig
            fs = stats.get(runconfig)
            if fs is None:
                stats[runconfig] = fs = RunFilterStatistic(filter=self.filter, runconfig=runconfig,
                                                           matched_count=0, covered_count=0)

            fs.covered_count += 1

        # Now that we know which results are covered, we just need to refine our
        # query to also check if they matched.
        #
        # To avoid asking the database to re-do the coverage test, just use the
        # list of ids we got previously
        query = TestResult.from_user_filters(**self.filter._to_user_query(covers=False, matches=True)).objects
        query = query.filter(id__in=[r.id for r in results]).only('ts_run').prefetch_related('ts_run__runconfig')
        for result in query:
            stats[result.ts_run.runconfig].matched_count += 1

        # Save the statistics objects
        for fs in stats.values():
            fs.save()

    def update_statistics(self):
        # drop all the caches
        try:
            del self.__runfilter_stats_covered__
            del self.runconfigs_covered
            del self.runconfigs_affected
        except AttributeError:
            # Ignore the error if the cache had not been accessed before
            pass

        req = KnownFailure.objects.filter(matched_ifa=self, result__ts_run__runconfig__temporary=False)
        req = req.order_by("result__ts_run__runconfig__added_on")
        oldest_failure = req.values_list('result__ts_run__runconfig__added_on', flat=True).first()
        if oldest_failure is not None:
            self.covers_from = oldest_failure

        # get the list of runconfigs needing update
        self._add_missing_stats()

        self.runconfigs_covered_count = len(self.runconfigs_covered)
        self.runconfigs_affected_count = len(self.runconfigs_affected)

        # Find when the issue was last seen
        for stats in self.__runfilter_stats_covered__:
            if stats.matched_count > 0:
                self.last_seen = stats.runconfig.added_on
                self.last_seen_runconfig = stats.runconfig
                break

        # Update the statistics atomically in the DB
        cur_ifa = IssueFilterAssociated.objects.filter(id=self.id)
        cur_ifa.update(covers_from=self.covers_from,
                       runconfigs_covered_count=self.runconfigs_covered_count,
                       runconfigs_affected_count=self.runconfigs_affected_count,
                       last_seen=self.last_seen,
                       last_seen_runconfig=self.last_seen_runconfig)

    @property
    def failure_rate(self):
        return Rate("runs", self.runconfigs_affected_count, self.runconfigs_covered_count)

    @property
    def activity_period(self):
        added_by = " by {}".format(render_to_string("CIResults/basic/user.html",
                                                    {"user": self.added_by}).strip()) if self.added_by else ""
        deleted_by = " by {}".format(render_to_string("CIResults/basic/user.html",
                                                      {"user": self.deleted_by}).strip()) if self.deleted_by else ""

        if not self.active:
            s = "Added {}{}, removed {}{} (was active for {})"
            return s.format(naturaltime(self.added_on), added_by,
                            naturaltime(self.deleted_on), deleted_by,
                            timesince(self.added_on, self.deleted_on))
        else:
            return "Added {}{}".format(naturaltime(self.added_on), added_by)

    def __str__(self):
        if self.deleted_on is not None:
            delete_on = " - deleted on {}".format(self.deleted_on)
        else:
            delete_on = ""

        return "{} on {}{}".format(self.filter.description, self.issue, delete_on)


class Issue(models.Model, UserFiltrableMixin):
    filter_objects_to_db = {
        'filter_description': FilterObjectStr('filters__description',
                                              'Description of what the filter matches'),
        'filter_runconfig_tag_name': FilterObjectStr('filters__tags__name',
                                                     'Run configuration tag matched by the filter'),
        'filter_machine_tag_name': FilterObjectStr('filters__machine_tags__name',
                                                   'Machine tag matched by the filter'),
        'filter_machine_name': FilterObjectStr('filters__machines__name',
                                               'Name of a machine matched by the filter'),
        'filter_test_name': FilterObjectStr('filters__tests__name',
                                            'Name of a test matched by the filter'),
        'filter_status_name': FilterObjectStr('filters__statuses__name',
                                              'Status matched by the filter'),
        'filter_stdout_regex': FilterObjectStr('filters__stdout_regex',
                                               'Regular expression for the standard output used by the filter'),
        'filter_stderr_regex': FilterObjectStr('filters__stderr_regex',
                                               'Regular expression for the error output used by the filter'),
        'filter_dmesg_regex': FilterObjectStr('filters__dmesg_regex',
                                              'Regular expression for the kernel logs used by the filter'),
        'filter_added_on': FilterObjectDateTime('issuefilterassociated__added_on',
                                                'Date at which the filter was associated to the issue'),
        'filter_covers_from': FilterObjectDateTime('issuefilterassociated__covers_from',
                                                   'Date of the first failure covered by the filter'),
        'filter_deleted_on': FilterObjectDateTime('issuefilterassociated__deleted_on',
                                                  'Date at which the filter was deleted from the issue'),
        'filter_runconfigs_covered_count': FilterObjectInteger('issuefilterassociated__runconfigs_covered_count',
                                                               'Amount of run configurations covered by the filter'),
        'filter_runconfigs_affected_count': FilterObjectInteger('issuefilterassociated__runconfigs_affected_count',
                                                                'Amount of run configurations affected by the filter'),
        'filter_last_seen': FilterObjectDateTime('issuefilterassociated__last_seen',
                                                 'Date at which the filter last matched'),
        'filter_last_seen_runconfig_name': FilterObjectStr('issuefilterassociated__last_seen_runconfig__name',
                                                           'Run configuration which last matched the filter'),

        'bug_tracker_name': FilterObjectStr('bugs__tracker__name',
                                            'Name of the tracker hosting the bug associated to the issue'),
        'bug_tracker_short_name': FilterObjectStr('bugs__tracker__short_name',
                                                  'Short name of the tracker hosting the bug associated to the issue'),
        'bug_tracker_type': FilterObjectStr('bugs__tracker__tracker_type',
                                            'Type of tracker hosting the bug associated to the issue'),
        'bug_id': FilterObjectStr('bugs__bug_id',
                                  'ID of the bug associated to the issue'),
        'bug_title': FilterObjectStr('bugs__title',
                                     'Title of the bug associated to the issue'),
        'bug_created_on': FilterObjectDateTime('bugs__created',
                                               'Date at which the bug associated to the issue was created'),
        'bug_updated_on': FilterObjectDateTime('bugs__updated',
                                               'Date at which the bug associated to the issue was last updated'),
        'bug_closed_on': FilterObjectDateTime('bugs__closed',
                                              'Date at which the bug associated to the issue was closed'),
        'bug_creator_name': FilterObjectStr('bugs__creator__person__full_name',
                                            'Name of the creator of the bug associated to the issue'),
        'bug_creator_email': FilterObjectStr('bugs__creator__person__email',
                                             'Email address of the creator of the bug associated to the issue'),
        'bug_assignee_name': FilterObjectStr('bugs__assignee__person__full_name',
                                             'Name of the assignee of the bug associated to the issue'),
        'bug_assignee_email': FilterObjectStr('bugs__assignee__person__email',
                                              'Email address of the assignee of the bug associated to the issue'),
        'bug_product': FilterObjectStr('bugs__product', 'Product of the bug associated to the issue'),
        'bug_component': FilterObjectStr('bugs__component', 'Component of the bug associated to the issue'),
        'bug_priority': FilterObjectStr('bugs__priority', 'Priority of the bug associated to the issue'),
        'bug_features': FilterObjectStr('bugs__features',
                                        'Features of the bug associated to the issue (coma-separated list)'),
        'bug_platforms': FilterObjectStr('bugs__platforms',
                                         'Platforms of the bug associated to the issue (coma-separated list)'),
        'bug_status': FilterObjectStr('bugs__status',
                                      'Status of the bug associated to the issue'),
        'bug_tags': FilterObjectStr('bugs__tags',
                                    'Tags/labels on the bug associated to this issue (coma-separated list)'),

        'description': FilterObjectStr('description', 'Free-hand text associated to the issue by the bug filer'),
        'filer_email': FilterObjectStr('filer', 'Email address of the person who filed the issue (DEPRECATED)'),

        'added_on': FilterObjectDateTime('added_on', 'Date at which the issue was created'),
        'added_by': FilterObjectStr('added_by__username', 'Username of the person who filed the issue'),
        'archived_on': FilterObjectDateTime('archived_on', 'Date at which the issue was archived'),
        'archived_by': FilterObjectStr('archived_by__username', 'Username of the person who archived the issue'),
        'expected': FilterObjectBool('expected', 'Is the issue expected?'),
        'runconfigs_covered_count': FilterObjectInteger('runconfigs_covered_count',
                                                        'Amount of run configurations covered by the issue'),
        'runconfigs_affected_count': FilterObjectInteger('runconfigs_affected_count',
                                                         'Amount of run configurations affected by the issue'),
        'last_seen': FilterObjectDateTime('last_seen', 'Date at which the issue was last seen'),
        'last_seen_runconfig_name': FilterObjectStr('last_seen_runconfig__name',
                                                    'Run configuration which last reproduced the issue'),
    }

    filters = models.ManyToManyField(IssueFilter, through="IssueFilterAssociated")
    bugs = models.ManyToManyField(Bug)

    description = models.TextField(blank=True)
    filer = models.EmailField()  # DEPRECATED

    added_on = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='issue_creator',
                                 null=True, on_delete=models.SET(get_sentinel_user))

    archived_on = models.DateTimeField(blank=True, null=True, db_index=True)
    archived_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='issue_archiver',
                                    null=True, on_delete=models.SET(get_sentinel_user))

    expected = models.BooleanField(default=False, db_index=True,
                                   help_text="Is this issue expected and should be considered an active issue?")

    # Statistics cache
    runconfigs_covered_count = models.PositiveIntegerField(default=0)
    runconfigs_affected_count = models.PositiveIntegerField(default=0)
    last_seen = models.DateTimeField(null=True, blank=True)
    last_seen_runconfig = models.ForeignKey('RunConfig', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        permissions = [
            ("archive_issue", "Can archive issues"),
            ("restore_issue", "Can restore issues"),

            ("hide_issue", "Can hide / mark as un-expected"),
            ("show_issue", "Can show / mark as as expected"),
        ]

    @property
    def archived(self):
        return self.archived_on is not None

    def hide(self):
        self.expected = True
        self.save()

    def show(self):
        self.expected = False
        self.save()

    @cached_property
    def active_filters(self):
        if self.archived:
            deleted_on = self.archived_on
        else:
            deleted_on = None

        if hasattr(self, 'ifas_cached'):
            ifas = filter(lambda i: i.deleted_on == deleted_on, self.ifas_cached)
            return sorted(ifas, reverse=True, key=lambda i: i.id)
        else:
            ifas = IssueFilterAssociated.objects.filter(issue=self,
                                                        deleted_on=deleted_on)
            return ifas.select_related("filter").order_by('-id')

    @cached_property
    def all_filters(self):
        return IssueFilterAssociated.objects.filter(issue=self).select_related('filter').order_by('id')

    @property
    def past_filters(self):
        return [ifa for ifa in self.all_filters if ifa.deleted_on != self.archived_on]

    @cached_property
    def bugs_cached(self):
        # HACK: Sort by decreasing ID in python so as we can prefetch the bugs
        # in the main view, saving as many SQL requests as we have bugs
        return sorted(self.bugs.all(), reverse=True, key=lambda b: b.id)

    @cached_property
    def covers_from(self):
        return min([self.added_on] + [min(ifa.added_on, ifa.covers_from) for ifa in self.all_filters])

    @cached_property
    def __runfilter_stats_covered__(self):
        filters = [e.filter for e in self.all_filters]
        objs = RunFilterStatistic.objects.select_related("runconfig")
        objs = objs.filter(runconfig__added_on__gte=self.covers_from,
                           covered_count__gt=0,
                           filter__in=filters).order_by("-runconfig__added_on")
        if self.archived:
            objs = objs.filter(runconfig__added_on__lt=self.archived_on)

        return objs

    @cached_property
    def runconfigs_covered(self):
        return set([r.runconfig for r in self.__runfilter_stats_covered__])

    @cached_property
    def runconfigs_affected(self):
        # Go through all the RunFilterStats covered by this issue and add runs
        # to the set of affected ones
        runconfigs_affected = set()
        for runfilter in self.__runfilter_stats_covered__:
            if runfilter.matched_count > 0:
                runconfigs_affected.add(runfilter.runconfig)

        return runconfigs_affected

    def update_statistics(self):
        self.runconfigs_covered_count = len(self.runconfigs_covered)
        self.runconfigs_affected_count = len(self.runconfigs_affected)

        # Find when the issue was last seen
        for stats in self.__runfilter_stats_covered__:
            if stats.matched_count > 0:
                self.last_seen = stats.runconfig.added_on
                self.last_seen_runconfig = stats.runconfig
                break

        # Update the statistics atomically in the DB
        cur_issue = Issue.objects.filter(id=self.id)
        cur_issue.update(runconfigs_covered_count=self.runconfigs_covered_count,
                         runconfigs_affected_count=self.runconfigs_affected_count,
                         last_seen=self.last_seen,
                         last_seen_runconfig=self.last_seen_runconfig)

    @property
    def failure_rate(self):
        return Rate("runs", self.runconfigs_affected_count, self.runconfigs_covered_count)

    def matches(self, result):
        if self.archived:
            return False

        for e in IssueFilterAssociated.objects_ready_for_matching.filter(deleted_on=None):
            if e.filter.matches(result):
                return True
        return False

    def archive(self, user):
        if self.archived:
            raise ValueError("The issue is already archived")

        with transaction.atomic():
            now = timezone.now()
            for e in IssueFilterAssociated.objects.filter(issue=self):
                e.delete(user, now)
            self.archived_on = now
            self.archived_by = user
            self.save()

        # Post a comment
        comment = render_to_string("CIResults/issue_archived.txt", {"issue": self})
        self.comment_on_all_bugs(comment)

    def restore(self):
        if not self.archived:
            raise ValueError("The issue is not currently archived")

        # re-add all the filters that used to be associated
        with transaction.atomic():
            for e in IssueFilterAssociated.objects.filter(issue=self, deleted_on=self.archived_on):
                self.__filter_add__(e.filter, e.added_by)

            # Mark the issue as not archived anymore before saving the changes
            self.archived_on = None
            self.archived_by = None
            self.save()

        # Post a comment
        comment = render_to_string("CIResults/issue_restored.txt", {"issue": self})
        self.comment_on_all_bugs(comment)

    @transaction.atomic
    def set_bugs(self, bugs):
        if self.archived:
            raise ValueError("The issue is archived, and thus read-only")

        # Let's simply delete all the bugs before adding them back
        self.bugs.clear()

        for bug in bugs:
            # Make sure the bug exists in the database first
            if bug.id is None:
                bug.save()

            # Add it to the relation
            self.bugs.add(bug)

        # Get rid of the cached bugs
        try:
            del self.bugs_cached
        except AttributeError:
            # Ignore the error if the cached had not been accessed before
            pass

    def __filter_add__(self, filter, user):
        # Make sure the filter exists in the database first
        if filter.id is None:
            filter.save()

        # Create the association between the filter and the issue
        ifa = IssueFilterAssociated.objects.create(filter=filter, issue=self, added_by=user)

        # Go through the untracked issues and check if the filter matches any of
        # them. Also include the unknown failures from temporary runs.
        now = timezone.now()
        new_matched_failures = []
        for failure in UnknownFailure.objects.prefetch_related('result',
                                                               'result__ts_run__runconfig__tags',
                                                               'result__ts_run__machine',
                                                               'result__test',
                                                               'result__status').all():
            if filter.matches(failure.result):
                filing_delay = now - failure.result.ts_run.reported_on
                kf = KnownFailure.objects.create(result=failure.result, matched_ifa=ifa,
                                                 manually_associated_on=now,
                                                 filing_delay=filing_delay)
                new_matched_failures.append(kf)
                failure.delete()

        ifa.update_statistics()

        return new_matched_failures

    def comment_on_all_bugs(self, comment):
        comment += ""  # Add an empty string to get a string instead of safetext

        try:
            for bug in self.bugs_cached:
                bug.add_comment(comment)
        except Exception:          # pragma: no cover
            traceback.print_exc()  # pragma: no cover

    def replace_filter(self, old_filter, new_filter, user):
        if self.archived:
            raise ValueError("The issue is archived, and thus read-only")

        with transaction.atomic():
            # First, add the new filter
            failures = self.__filter_add__(new_filter, user)
            new_matched_failures = [f for f in failures if not f.result.ts_run.runconfig.temporary]

            # Delete all active associations of the old filter
            assocs = IssueFilterAssociated.objects.filter(deleted_on=None, filter=old_filter)
            for e in assocs:
                e.delete(user, timezone.now())

            # Now update our statistics since we possibly re-assigned some new failures
            self.update_statistics()

        # Post a comment on the bugs associated to this issue if something changed
        if (old_filter.description != new_filter.description or
           old_filter.to_user_query != new_filter.to_user_query or
           len(new_matched_failures) > 0):
            comment = render_to_string("CIResults/issue_replace_filter_comment.txt",
                                       {"issue": self, "old_filter": old_filter,
                                        "new_filter": new_filter, "new_matched_failures": new_matched_failures,
                                        "user": user})
            self.comment_on_all_bugs(comment)

    def set_filters(self, filters, user):
        if self.archived:
            raise ValueError("The issue is archived, and thus read-only")

        with transaction.atomic():
            removed_ifas = set()
            new_filters = dict()

            # Query the set of issues that we currently have
            assocs = IssueFilterAssociated.objects.filter(deleted_on=None, issue=self)

            # First, "delete" all the filters that are not in the new set
            now = timezone.now()
            for e in assocs:
                if e.filter not in filters:
                    e.delete(user, now)
                    removed_ifas.add(e)

            # Now, let's add all the new ones
            cur_filters_ids = set([e.filter.id for e in assocs])
            for filter in filters:
                if filter.id not in cur_filters_ids:
                    new_filters[filter] = self.__filter_add__(filter, user)

            # Now update our statistics since we possibly re-assigned some new failures
            self.update_statistics()

        # Get rid of the cached filters
        try:
            del self.active_filters
        except AttributeError:
            # Ignore the error if the cache had not been accessed before
            pass

        # Post a comment on the bugs associated to this issue
        if len(removed_ifas) > 0 or len(new_filters) > 0:
            comment = render_to_string("CIResults/issue_set_filters_comment.txt",
                                       {"issue": self, "removed_ifas": removed_ifas,
                                        "new_filters": new_filters, "user": user})
            self.comment_on_all_bugs(comment + "")  # Add an empty string to get a string instead of safetext

    @transaction.atomic
    def merge_issues(self, issues, user):
        # TODO: This is just a definition of interface, the code is untested

        # First, add all our current filters to a list
        new_issue_filters = [filter for filter in self.filters.all()]

        # Collect the list of filters from the issues we want to merge before
        # archiving them
        for issue in issues:
            for filter in issue.filters.all():
                new_issue_filters.append(filter)
            issue.archive(user)

        # Set the new list of filters
        self.set_filters(new_issue_filters, user)

        # Now update our statistics since we possibly re-assigned some new failures
        self.update_statistics()

    def __str__(self):
        bugs = self.bugs.all()
        if len(bugs) == 0:
            return "Issue: <empty>"
        elif len(bugs) == 1:
            return "Issue: " + str(bugs[0])
        else:
            return "Issue: [{}]".format(", ".join([b.short_name for b in bugs]))


class KnownFailure(models.Model, UserFiltrableMixin):
    # For the FilterMixin.
    filter_objects_to_db = {
        'runconfig': FilterObjectModel(RunConfig, 'result__ts_run__runconfig', 'Run configuration the test is part of'),
        'runconfig_name': FilterObjectStr('result__ts_run__runconfig__name', 'Name of the run configuration'),
        'runconfig_tag': FilterObjectStr('result__ts_run__runconfig__tags__name',
                                         'Tag associated with the configuration used for this test execution'),
        'runconfig_added_on': FilterObjectDateTime('result__ts_run__runconfig__added_on',
                                                   'Date at which the run configuration got created'),
        'runconfig_temporary': FilterObjectBool('result__ts_run__runconfig__temporary',
                                                'Is the run configuration temporary, like for pre-merge testing?'),
        'build_name': FilterObjectStr('result__ts_run__runconfig__builds__name',
                                      'Name of the build for a component used for this test execution'),
        'component_name': FilterObjectStr('result__ts_run__runconfig__builds__component__name',
                                          'Name of a component used for this test execution'),
        'machine_name': FilterObjectStr('result__ts_run__machine__name', 'Name of the machine used for this result'),
        'machine_tag': FilterObjectStr('result__ts_run__machine__tags__name',
                                       'Tag associated to the machine used in this run'),
        'status_name': FilterObjectStr('result__status__name', 'Name of the resulting status (pass/fail/crash/...)'),
        'testsuite_name': FilterObjectStr('result__status__testsuite__name',
                                          'Name of the testsuite that contains this test'),
        'test_name': FilterObjectStr('result__test__name', 'Name of the test'),
        'test_added_on': FilterObjectDateTime('result__test__added_on', 'Date at which the test got added'),
        'manually_filed_on': FilterObjectDateTime('manually_associated_on',
                                                  'Date at which the failure got manually associated to an issue'),
        'ifa_id': FilterObjectInteger('matched_ifa_id',
                                      'ID of the associated filter that matched the failure'),
        'issue_id': FilterObjectInteger('matched_ifa__issue_id',
                                        'ID of the issue associated to the failure'),
        'issue_expected': FilterObjectBool('matched_ifa__issue__expected',
                                           'Is the issue associated to the failure marked as expected?'),
        'filter_description': FilterObjectStr('matched_ifa__issue__filters__description',
                                              'Description of what the filter associated to the failure'),
        'filter_runconfig_tag_name':
            FilterObjectStr('matched_ifa__issue__filters__tags__name',
                            'Run configuration tag matched by the filter associated to the failure'),
        'filter_machine_tag_name': FilterObjectStr('matched_ifa__issue__filters__machine_tags__name',
                                                   'Machine tag matched by the filter associated to the failure'),
        'filter_machine_name': FilterObjectStr('matched_ifa__issue__filters__machines__name',
                                               'Name of a machine matched by the filter associated to the failure'),
        'filter_test_name': FilterObjectStr('matched_ifa__issue__filters__tests__name',
                                            'Name of a test matched by the filter associated to the failure'),
        'filter_status_name': FilterObjectStr('matched_ifa__issue__filters__statuses__name',
                                              'Status matched by the filter associated to the failure'),
        'filter_stdout_regex': FilterObjectStr('matched_ifa__issue__filters__stdout_regex',
                                               'Standard output regex used by the filter associated to the failure'),
        'filter_stderr_regex': FilterObjectStr('matched_ifa__issue__filters__stderr_regex',
                                               'Standard error regex used by the filter associated to the failure'),
        'filter_dmesg_regex': FilterObjectStr('matched_ifa__issue__filters__dmesg_regex',
                                              'Regex for dmesg used by the filter associated to the failure'),
        'filter_added_on': FilterObjectDateTime('matched_ifa__issue__issuefilterassociated__added_on',
                                                'Date at which the filter associated to the failure was added on to its issue'),  # noqa
        'filter_covers_from':
            FilterObjectDateTime('matched_ifa__issue__issuefilterassociated__covers_from',
                                 'Date of the first failure covered by the filter associated to the failure'),
        'filter_deleted_on':
            FilterObjectDateTime('matched_ifa__issue__issuefilterassociated__deleted_on',
                                 'Date at which the filter was removed from the issue associated to the failure'),
        'filter_runconfigs_covered_count':
            FilterObjectInteger('matched_ifa__issue__issuefilterassociated__runconfigs_covered_count',
                                'Amount of run configurations covered by the filter associated to the failure'),
        'filter_runconfigs_affected_count':
            FilterObjectInteger('matched_ifa__issue__issuefilterassociated__runconfigs_affected_count',
                                'Amount of run configurations affected by the filter associated to the failure'),
        'filter_last_seen':
            FilterObjectDateTime('matched_ifa__issue__issuefilterassociated__last_seen',
                                 'Date at which the filter matching this failure was last seen'),
        'filter_last_seen_runconfig_name':
            FilterObjectStr('matched_ifa__issue__issuefilterassociated__last_seen_runconfig__name',
                            'Run configuration which last matched the filter associated to the failure'),
        'bug_tracker_name': FilterObjectStr('matched_ifa__issue__bugs__tracker__name',
                                            'Name of the tracker which holds the bug associated to this failure'),
        'bug_tracker_short_name': FilterObjectStr('matched_ifa__issue__bugs__tracker__short_name',
                                                  'Short name of the tracker which holds the bug associated to this failure'),  # noqa
        'bug_tracker_type': FilterObjectStr('matched_ifa__issue__bugs__tracker__tracker_type',
                                            'Type of the tracker which holds the bug associated to this failure'),
        'bug_id': FilterObjectStr('matched_ifa__issue__bugs__bug_id',
                                  'ID of the bug associated to this failure'),
        'bug_title': FilterObjectStr('matched_ifa__issue__bugs__title',
                                     'Title of the bug associated to this failure'),
        'bug_created_on': FilterObjectDateTime('matched_ifa__issue__bugs__created',
                                               'Date at which the bug associated to this failure was created'),
        'bug_closed_on': FilterObjectDateTime('matched_ifa__issue__bugs__closed',
                                              'Date at which the bug associated to this failure was closed'),
        'bug_creator_name': FilterObjectStr('matched_ifa__issue__bugs__creator__person__full_name',
                                            'Name of the creator of the bug associated to this failure'),
        'bug_creator_email': FilterObjectStr('matched_ifa__issue__bugs__creator__person__email',
                                             'Email address of the creator of the bug associated to this failure'),
        'bug_assignee_name': FilterObjectStr('matched_ifa__issue__bugs__assignee__person__full_name',
                                             'Name of the assignee of the bug associated to this failure'),
        'bug_assignee_email': FilterObjectStr('matched_ifa__issue__bugs__assignee__person__email',
                                              'Email address of the assignee of the bug associated to this failure'),
        'bug_product': FilterObjectStr('matched_ifa__issue__bugs__product',
                                       'Product of the bug associated to this failure'),
        'bug_component': FilterObjectStr('matched_ifa__issue__bugs__component',
                                         'Component of the bug associated to this failure'),
        'bug_priority': FilterObjectStr('matched_ifa__issue__bugs__priority',
                                        'Priority of the bug associated to this failure'),
        'bug_features': FilterObjectStr('matched_ifa__issue__bugs__features',
                                        'Features of the bug associated to this failure (coma-separated list)'),
        'bug_platforms': FilterObjectStr('matched_ifa__issue__bugs__platforms',
                                         'Platforms of the bug associated to this failure (coma-separated list)'),
        'bug_status': FilterObjectStr('matched_ifa__issue__bugs__status',
                                      'Status of the bug associated to this failure'),
        'bug_tags': FilterObjectStr('matched_ifa__issue__bugs__tags',
                                    'Tags/labels on the bug associated to this failure (coma-separated list)'),
        'url': FilterObjectStr('result__url', 'External URL of this test result'),
        'start': FilterObjectDateTime('result__start', 'Date at which this test started being executed'),
        'duration': FilterObjectDuration('result__duration', 'Time it took to execute the test'),
        'command': FilterObjectStr('result__command', 'Command used to execute the test'),
        'stdout': FilterObjectStr('result__stdout', 'Standard output of the test execution'),
        'stderr': FilterObjectStr('result__stderr', 'Error output of the test execution'),
        'dmesg': FilterObjectStr('result__dmesg', 'Kernel logs of the test execution'),
    }

    result = models.ForeignKey(TestResult, on_delete=models.CASCADE,
                               related_name="known_failures", related_query_name="known_failure")
    matched_ifa = models.ForeignKey(IssueFilterAssociated, on_delete=models.CASCADE)

    # When was the mapping done (useful for metrics)
    manually_associated_on = models.DateTimeField(null=True, blank=True, db_index=True)
    filing_delay = models.DurationField(null=True, blank=True)

    @classmethod
    def _runconfig_index(cls, covered_list, runconfig):
        try:
            covered = sorted(covered_list, key=lambda r: r.added_on, reverse=True)
            return covered.index(runconfig)
        except ValueError:
            return None

    @cached_property
    def covered_runconfigs_since_for_issue(self):
        return self._runconfig_index(self.matched_ifa.issue.runconfigs_covered,
                                     self.result.ts_run.runconfig)

    @cached_property
    def covered_runconfigs_since_for_filter(self):
        return self._runconfig_index(self.matched_ifa.runconfigs_covered,
                                     self.result.ts_run.runconfig)

    def __str__(self):
        return "{} associated on {}".format(str(self.result), self.manually_associated_on)


class UnknownFailure(models.Model):
    # We cannot have two UnknownFailure for the same result
    result = models.OneToOneField(TestResult, on_delete=models.CASCADE,
                                  related_name="unknown_failure")

    matched_archived_ifas = models.ManyToManyField(IssueFilterAssociated)

    @cached_property
    def matched_archived_ifas_cached(self):
        return self.matched_archived_ifas.all()

    @cached_property
    def matched_issues(self):
        issues = set()
        for e in self.matched_archived_ifas_cached:
            issues.add(e.issue)
        return issues

    def __str__(self):
        return str(self.result)


# Allows us to know if a filter covers/matches a runconfig or not
class RunFilterStatistic(models.Model):
    runconfig = models.ForeignKey(RunConfig, on_delete=models.CASCADE)
    filter = models.ForeignKey(IssueFilter, on_delete=models.CASCADE)

    covered_count = models.PositiveIntegerField()
    matched_count = models.PositiveIntegerField()

    class Meta:
        unique_together = ('runconfig', 'filter')

    def __str__(self):
        if self.covered_count > 0:
            perc = self.matched_count * 100 / self.covered_count
        else:
            perc = 0
        return "{} on {}: match rate {}/{} ({:.2f}%)".format(self.filter,
                                                             self.runconfig,
                                                             self.matched_count,
                                                             self.covered_count,
                                                             perc)
