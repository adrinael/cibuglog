# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-05-11 17:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0012_remove_bug_comments'),
    ]

    operations = [
        migrations.CreateModel(
            name='BugComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment_id', models.CharField(help_text='The ID of the comment', max_length=20)),
                ('created_on', models.DateTimeField()),
                ('bug', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CIResults.Bug')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(blank=True, help_text='Full name of the person', max_length=100, null=True)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AddField(
            model_name='bugcomment',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CIResults.Person'),
        ),
        migrations.AddField(
            model_name='bug',
            name='comments_polled',
            field=models.DateTimeField(blank=True, help_text='Last time the comments of the bug were polled. To be filled automatically.', null=True),
        ),
        migrations.AddField(
            model_name='bug',
            name='comments',
            field=models.ManyToManyField(through='CIResults.BugComment', to='CIResults.Person'),
        ),
        migrations.AlterUniqueTogether(
            name='bugcomment',
            unique_together=set([('bug', 'comment_id')]),
        ),
    ]
