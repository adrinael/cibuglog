from unittest.mock import patch, MagicMock, PropertyMock
from django.test import TestCase
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

from CIResults.models import BugTracker, Bug, BugTrackerSLA, Person, BugTrackerAccount, BugComment
from CIResults.models import Test, Machine, RunConfigTag, RunConfig, TestSuite, TextStatus
from CIResults.models import TestResult, IssueFilter, IssueFilterAssociated, Issue, RunFilterStatistic
from CIResults.models import MachineTag, TestsuiteRun, UnknownFailure, KnownFailure, Component, Build
from CIResults.models import Rate, ReplicationScript, script_validator

import datetime
import pytz


class BugTrackerSLATests(TestCase):
    def test___str__(self):
        tracker = BugTracker(name="BugTracker")
        entry = BugTrackerSLA(tracker=tracker, priority="high", SLA=datetime.timedelta(weeks=10, seconds=23))
        self.assertEqual(str(entry), "BugTracker: high -> 70 days, 0:00:23")


class PersonTests(TestCase):
    def test_full(self):
        p = Person(full_name="John Doe", email="john.doe@isp.earth")
        self.assertEqual(str(p), "John Doe <john.doe@isp.earth>")

    def test_full_name_only_only(self):
        p = Person(full_name="John Doe")
        self.assertEqual(str(p), "John Doe")

    def test_email_only(self):
        p = Person(email="john.doe@isp.earth")
        self.assertEqual(str(p), "john.doe@isp.earth")

    def test_no_information(self):
        p = Person()
        self.assertEqual(str(p), "(No name or email)")


class TestBugTrackerAccount(TestCase):
    def test_str(self):
        person = Person(full_name="John Doe", email="john.doe@isp.earth")
        self.assertEqual(str(BugTrackerAccount(person=person)), str(person))


class BugTrackerTests(TestCase):
    def test_str(self):
        tracker = BugTracker(name="Freedesktop.org", short_name="fdo",
                                  separator="#", public=True,
                                  url="https://bugs.freedesktop.org/",
                                  bug_base_url="https://bugs.freedesktop.org/show_bug.cgi?id=")
        self.assertEqual(str(tracker), "Freedesktop.org")

    @patch('CIResults.models.BugTrackerSLA.objects.filter',
           return_value=[MagicMock(priority="P1", SLA=datetime.timedelta(seconds=1)),
                         MagicMock(priority="P2", SLA=datetime.timedelta(seconds=3)),
                         MagicMock(priority="P3", SLA=datetime.timedelta(seconds=2))])
    def test_SLAs_cached(self, filter_mocked):
        tracker = BugTracker(name="Tracker1", public=True)
        slas = tracker.SLAs_cached

        filter_mocked.assert_called_with(tracker=tracker)
        self.assertEqual(slas, {"p1": filter_mocked.return_value[0].SLA,
                                "p2": filter_mocked.return_value[1].SLA,
                                "p3": filter_mocked.return_value[2].SLA})

    @patch('CIResults.models.BugTracker.tracker')
    def test_poll(self, tracker_mock):
        bug = MagicMock()
        bug.save = MagicMock()

        BugTracker().poll(bug)
        tracker_mock.poll.assert_called_with(bug, False)
        bug.save.assert_not_called()
        self.assertLess(timezone.now() - bug.polled, datetime.timedelta(seconds=.1))

    @patch('CIResults.bugtrackers.Bugzilla')
    def test_tracker__bugzilla(self, bugzilla_mock):
        self.assertEqual(BugTracker(tracker_type="bugzilla").tracker, bugzilla_mock.return_value)

    @patch('CIResults.bugtrackers.Jira')
    def test_tracker__jira(self, jira_mock):
        self.assertEqual(BugTracker(tracker_type="jira").tracker, jira_mock.return_value)

    @patch('CIResults.bugtrackers.GitLab')
    def test_tracker__gitlab(self, gitlab_mock):
        self.assertEqual(BugTracker(tracker_type="gitlab").tracker, gitlab_mock.return_value)

    @patch('CIResults.bugtrackers.Untracked')
    def test_tracker__jira_untracked(self, untracked_mock):
        self.assertEqual(BugTracker(tracker_type="jira_untracked").tracker, untracked_mock.return_value)

    def test_tracker__invalid_name(self):
        tracker = BugTracker(tracker_type="invalid_name")
        self.assertRaisesMessage(ValueError, "The bugtracker type 'invalid_name' is unknown",
                                 getattr, tracker, "tracker")

    @patch.object(BugTracker, 'tracker', PropertyMock())
    def test_open_statuses(self):
        tracker = BugTracker(tracker_type="tracker")
        self.assertEqual(tracker.open_statuses, BugTracker.tracker.open_statuses)

    @patch('CIResults.models.Bug.objects.filter', return_value=[MagicMock(), MagicMock(), MagicMock()])
    @patch('CIResults.models.BugTracker.poll')
    def test_poll_all(self, poll_mocked, bugs_mocked):
        tracker = BugTracker(public=True)
        tracker.poll_all()

        # Check that filter got called with the right argument
        bugs_mocked.assert_called_with(tracker=tracker)

        # Check that every bug we set up got polled
        self.assertEqual(poll_mocked.call_count, 3)
        for i in range(3):
            poll_mocked.assert_any_call(bugs_mocked.return_value[i])

        self.assertLess(abs((timezone.now() - tracker.polled).total_seconds()), .1)

    @patch('CIResults.models.BugTracker.poll')
    def test_poll_all__custom_list(self, poll_mocked):
        bugs = [MagicMock(), MagicMock(), MagicMock()]

        tracker = BugTracker(public=True)
        tracker.poll_all(bugs=bugs)

        # Check that every bug we set up got polled
        self.assertEqual(poll_mocked.call_count, 3)
        for i in range(3):
            poll_mocked.assert_any_call(bugs[i])

        self.assertLess(abs((timezone.now() - tracker.polled).total_seconds()), 1)

    @patch('CIResults.models.Bug.objects.filter', return_value=[MagicMock(), MagicMock(), MagicMock()])
    @patch('CIResults.models.BugTracker.poll')
    def test_poll_all_interrupt(self, poll_mocked, bugs_mocked):
        tracker = BugTracker()

        stop_event = MagicMock(is_set=MagicMock(side_effect=[False, False, True]))
        tracker.poll_all(stop_event)

        # Check that filter got called with the right argument
        bugs_mocked.assert_called_with(tracker=tracker)

        # Check that every bug we set up got polled
        self.assertEqual(poll_mocked.call_count, 2)
        for i in range(2):
            poll_mocked.assert_any_call(bugs_mocked.return_value[i])

    def test_components_followed_list(self):
        tracker = BugTracker(public=True)
        self.assertEqual(tracker.components_followed_list, [])

        tracker.components_followed = "COMPONENT1,COMPONENT2, COMPONENT3"
        self.assertEqual(tracker.components_followed_list, ["COMPONENT1", "COMPONENT2", "COMPONENT3"])

    def test_get_or_create_bugs(self):
        tracker = BugTracker.objects.create(name='tracker', tracker_type='jira_untracked', public=True)
        Bug.objects.create(tracker=tracker, bug_id='1')
        Bug.objects.create(tracker=tracker, bug_id='3')

        self.assertEqual(len(Bug.objects.all()), 2)
        tracker.get_or_create_bugs(set(['1', '2', '3']))
        self.assertEqual(len(Bug.objects.all()), 3)

    @patch('CIResults.models.BugTracker.open_statuses', new_callable=PropertyMock)
    def test_is_bug_open(self, open_statuses_mock):
        open_statuses_mock.return_value = ["status1", "status3"]
        tracker = BugTracker(public=True)
        bug_status1 = Bug(tracker=tracker, status="status1")
        bug_status2 = Bug(tracker=tracker, status="status2")
        bug_status3 = Bug(tracker=tracker, status="status3")
        bug_no_status = Bug(tracker=tracker, status=None)

        for bug, is_open in [(bug_status1, True), (bug_status2, False), (bug_status3, True),
                             (bug_no_status, False)]:
            self.assertEqual(tracker.is_bug_open(bug), is_open,
                             "{}: Should be opened? {}".format(bug.status, is_open))

    @patch('CIResults.models.Bug.objects.filter', return_value=set([MagicMock(spec=Bug, bug_id='existing1'),
                                                                    MagicMock(spec=Bug, bug_id='existing2')]))
    def test_open_bugs__without_followed_list(self, bugs_filter_mocked):
        tracker = BugTracker.objects.create(name='tracker', tracker_type='jira_untracked', public=True)

        self.assertEqual(tracker.open_bugs(), bugs_filter_mocked.return_value)
        bugs_filter_mocked.assert_called_with(tracker=tracker, status__in=tracker.open_statuses)

    @patch('CIResults.models.BugTracker.get_or_create_bugs', return_value=set([MagicMock(spec=Bug,
                                                                                         bug_id='searched_bug')]))
    @patch('CIResults.bugtrackers.Untracked.search_bugs_ids')
    @patch('CIResults.models.Bug.objects.filter', return_value=set([MagicMock(spec=Bug, bug_id='existing1'),
                                                                    MagicMock(spec=Bug, bug_id='existing2')]))
    def test_open_bugs__with_followed_list(self, bugs_filter_mocked, search_bugs_ids_mocked,
                                           get_or_create_bugs_mocked):
        tracker = BugTracker.objects.create(name='tracker', tracker_type='jira_untracked', public=True,
                                            components_followed="product1,product2")

        self.assertEqual(tracker.open_bugs(), bugs_filter_mocked.return_value | get_or_create_bugs_mocked.return_value)
        bugs_filter_mocked.assert_called_with(tracker=tracker, status__in=tracker.open_statuses)
        search_bugs_ids_mocked.assert_called_with(components=["product1", "product2"], status=tracker.open_statuses)

    @patch('CIResults.models.BugTracker.open_bugs')
    def test_followed_bugs(self, open_bugs_mock):
        tracker1 = BugTracker.objects.create(name='tracker1', tracker_type='jira_untracked', public=True)
        tracker2 = BugTracker.objects.create(name='tracker2', tracker_type='jira_untracked', public=True)

        # Create the "open bugs"
        open_bugs_mock.return_value = set([Bug.objects.create(bug_id='1234', tracker=tracker1),
                                           Bug.objects.create(bug_id='1235', tracker=tracker1)])

        # Create the bugs associated to the issues
        bug1 = Bug.objects.create(bug_id='1', tracker=tracker1)
        bug2 = Bug.objects.create(bug_id='2', tracker=tracker2)
        bug_old_issue = Bug.objects.create(bug_id='OLD_ISSUE', tracker=tracker1)
        Bug.objects.create(bug_id='UNREFERENCED', tracker=tracker2)

        active = Issue.objects.create()
        active.bugs.add(bug1, bug2)

        archived = Issue.objects.create(archived_on=timezone.now())
        archived.bugs.add(bug2, bug_old_issue)

        # Make sure that only the expected bugs are returned for each tracker
        self.assertEqual(tracker1.followed_bugs(), set([bug1]) | open_bugs_mock.return_value)
        self.assertEqual(tracker2.followed_bugs(), set([bug2]) | open_bugs_mock.return_value)

    @patch('CIResults.models.BugTracker.get_or_create_bugs')
    @patch('CIResults.bugtrackers.Untracked.search_bugs_ids')
    @patch('CIResults.models.BugTracker.bugs_in_issues')
    @patch('CIResults.models.timezone')
    def test_updated_bugs(self, tz_mock, bugs_in_issues_mock, search_bugs_mock, get_or_create_mock):
        tracker1 = BugTracker.objects.create(name='tracker1', tracker_type='jira_untracked',
                                             public=True, components_followed="FOO")
        tracker1.tracker.open_statuses = ["Open"]
        tracker1.polled = datetime.datetime.fromtimestamp(0, pytz.utc)
        tracker1.tracker._get_tracker_time = MagicMock(return_value=tracker1.polled + datetime.timedelta(seconds=20))
        tz_mock.now.return_value = tracker1.polled + datetime.timedelta(seconds=10)

        all_upd_bugs_ids = set(["bug2", "bug3", "bug4"])
        open_bugs_ids = set(["bug1", "bug3"])

        issue_bug1 = Bug.objects.create(bug_id='bug4', status="Closed", tracker=tracker1)
        issue_bug2 = Bug.objects.create(bug_id='bug5', status="Closed", tracker=tracker1)

        Bug.objects.create(bug_id='bug1', status="Open", tracker=tracker1)
        Bug.objects.create(bug_id='bug2', status="Closed", tracker=tracker1)
        Bug.objects.create(bug_id='bug3', status="Open", tracker=tracker1)

        search_bugs_mock.side_effect = [all_upd_bugs_ids,
                                        open_bugs_ids]

        bugs_in_issues_mock.return_value = set([issue_bug1, issue_bug2])
        tracker1.updated_bugs()
        self.assertEqual(datetime.datetime.fromtimestamp(0, pytz.utc) + datetime.timedelta(seconds=10),
                         search_bugs_mock.call_args_list[0][1]['updated_since'])

        """
        bug1 - (not returned) open but not updated
        bug2 - (not returned) updated but not open
        bug3 - (returned)     updated and open
        bug4 - (returned)     not open, but updated and associated to issue
        bug5 - (not returned) not open or updated
        """
        get_or_create_mock.assert_called_with(set(["bug3", "bug4"]))


class BugTests(TestCase):
    Model = Bug

    def setUp(self):
        self.tracker = BugTracker(name="Freedesktop.org", short_name="fdo",
                                  separator="#", public=True,
                                  url="https://bugs.freedesktop.org/",
                                  bug_base_url="https://bugs.freedesktop.org/show_bug.cgi?id=")
        self.bug = Bug(tracker=self.tracker, bug_id="1234", title="random title",
                       created=timezone.now() - datetime.timedelta(days=4))

    def test_short_name(self):
        self.assertEqual(self.bug.short_name, "fdo#1234")

    def test_url(self):
        self.assertEqual(self.bug.url,
                         "https://bugs.freedesktop.org/show_bug.cgi?id=1234")

    def test_features_list(self):
        self.assertEqual(self.bug.features_list, [])
        self.bug.features = "feature1,feature2 , feature3"
        self.assertEqual(self.bug.features_list, ["feature1", "feature2", "feature3"])

    def test_platforms_list(self):
        self.assertEqual(self.bug.platforms_list, [])
        self.bug.platforms = "platform1,platform2 , platform3"
        self.assertEqual(self.bug.platforms_list, ["platform1", "platform2", "platform3"])

    def test_tags_list(self):
        self.assertEqual(self.bug.tags_list, [])
        self.bug.tags = "tag1,tag 2 , tag3"
        self.assertEqual(self.bug.tags_list, ["tag1", "tag 2", "tag3"])

    def test_has_new_comments(self):
        self.bug.updated = timezone.now()

        # comments_polled = None
        self.assertTrue(self.bug.has_new_comments)

        # comments_polled < updated
        self.bug.comments_polled = self.bug.updated - datetime.timedelta(seconds=1)
        self.assertTrue(self.bug.has_new_comments)

        # comments_polled > updated
        self.bug.comments_polled = self.bug.updated + datetime.timedelta(seconds=1)
        self.assertFalse(self.bug.has_new_comments)

    @patch('CIResults.models.BugComment.objects.filter')
    def test_comments_cached(self, filter_mock):
        self.assertEqual(self.bug.comments_cached, filter_mock.return_value.prefetch_related())
        filter_mock.assert_called_with(bug=self.bug)

    def test_SLA(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        sla_high = BugTrackerSLA.objects.create(tracker=tracker, priority="HIGH", SLA=datetime.timedelta(seconds=23))
        sla_low = BugTrackerSLA.objects.create(tracker=tracker, priority="low", SLA=datetime.timedelta(seconds=30))

        self.assertEqual(tracker.SLAs_cached, {"low": sla_low.SLA, "high": sla_high.SLA})

        bug = Bug(tracker=tracker, priority="low")
        self.assertEqual(bug.SLA, sla_low.SLA)

        bug = Bug(tracker=tracker, priority="LOW")
        self.assertEqual(bug.SLA, sla_low.SLA)

        bug = Bug(tracker=tracker, priority="invalid")
        self.assertEqual(bug.SLA, datetime.timedelta.max)

    def test_SLA_deadline__triage_needed(self):
        # Check that if no developer has updated the bug, our deadline is set to the tracker's first_response_SLA
        self.tracker.first_response_SLA = datetime.timedelta(days=2.1)
        self.assertEqual(self.bug.SLA_deadline, self.bug.created + self.tracker.first_response_SLA)

    def test_SLA_deadline__normal_SLA(self):
        # Check that when we have a developer comment, we follow the SLA
        self.bug.SLA = datetime.timedelta(days=1)
        self.bug.last_updated_by_developer = timezone.now()
        self.assertAlmostEqual(self.bug.SLA_deadline.timestamp(),
                               (self.bug.last_updated_by_developer + self.bug.SLA).timestamp(),
                               places=1)

    def test_SLA_deadline__infinite_SLA(self):
        # In the event where the SLA is infine, verify that we always set the deadline a year in advance
        self.bug.SLA = datetime.timedelta.max
        self.bug.last_updated_by_developer = timezone.now() - datetime.timedelta(days=30)
        self.assertAlmostEqual(self.bug.SLA_deadline.timestamp(),
                               (timezone.now() + datetime.timedelta(days=365, seconds=1)).timestamp(),
                               places=1)

    def test_SLA_remaining_time__one_day_left(self):
        self.bug.SLA_deadline = timezone.now() - datetime.timedelta(days=1)
        self.assertAlmostEqual(self.bug.SLA_remaining_time.total_seconds(),
                               datetime.timedelta(days=-1).total_seconds(), places=1)

    def test_SLA_remaining_time__one_day_over(self):
        self.bug.SLA_deadline = timezone.now() + datetime.timedelta(days=1)
        self.assertLessEqual(abs(self.bug.SLA_remaining_time.total_seconds() -
                                 datetime.timedelta(days=1).total_seconds()), 1)

    def test_SLA_remaining_str__one_day_over(self):
        self.bug.SLA_remaining_time = datetime.timedelta(days=-1)
        exp = "1 day, 0:00:00 ago"
        self.assertEqual(exp, self.bug.SLA_remaining_str)

    def test_SLA_remaining_str__one_day_left(self):
        self.bug.SLA_remaining_time = datetime.timedelta(days=1)
        exp = "in 1 day, 0:00:00"
        self.assertEqual(exp, self.bug.SLA_remaining_str)

    def test_effective_priority(self):
        self.bug.SLA_remaining_time = datetime.timedelta(days=3)
        self.bug.SLA = datetime.timedelta(hours=2.5)
        self.assertAlmostEqual(self.bug.effective_priority, -self.bug.SLA_remaining_time / self.bug.SLA)

    def test_is_being_updated__never_flagged(self):
        self.assertFalse(self.bug.is_being_updated)

    def test_is_being_updated__not_expired(self):
        self.bug.flagged_as_update_pending_on = timezone.now()
        self.assertTrue(self.bug.is_being_updated)

    def test_is_being_updated__expired(self):
        self.bug.flagged_as_update_pending_on = timezone.now() - self.bug.UPDATE_PENDING_TIMEOUT
        self.assertFalse(self.bug.is_being_updated)

    def test_update_pending_expires_in__never_flagged(self):
        self.assertEqual(self.bug.update_pending_expires_in, None)

    @patch('django.utils.timezone.now',
           return_value=datetime.datetime.strptime('2019-01-01T00:00:05', "%Y-%m-%dT%H:%M:%S"))
    def test_update_pending_expires_in__not_expired(self, now_mocked):
        self.bug.flagged_as_update_pending_on = timezone.now()
        self.assertEqual(self.bug.update_pending_expires_in, self.bug.UPDATE_PENDING_TIMEOUT)

    @patch('django.utils.timezone.now',
           return_value=datetime.datetime.strptime('2019-01-01T00:00:05', "%Y-%m-%dT%H:%M:%S"))
    def test_update_pending_expires_in__expired(self, now_mocked):
        self.bug.flagged_as_update_pending_on = timezone.now() - 2 * self.bug.UPDATE_PENDING_TIMEOUT
        self.assertEqual(self.bug.update_pending_expires_in, -self.bug.UPDATE_PENDING_TIMEOUT)

    @patch('CIResults.models.BugTracker.poll')
    def test_poll(self, poll_mock):
        self.bug.poll()
        poll_mock.assert_called_with(self.bug, False)

    def test_create(self):
        tracker = BugTracker.objects.create(name="Tracker2", tracker_type="jira", url="http://foo",
                                            project="TEST2", public=True)
        bug = Bug(tracker=tracker, title="random title")
        bug.tracker.tracker.create_bug = MagicMock(return_value=1)
        bug.create()
        self.assertEqual(bug.bug_id, 1)

    def test_create_error(self):
        tracker = BugTracker.objects.create(name="Tracker2", tracker_type="jira", url="http://foo",
                                            project="TEST2", public=True)
        bug = Bug(tracker=tracker, title="random title")
        bug.tracker.tracker.create_bug = MagicMock(side_effect=ValueError)
        bug.create()
        self.assertNotEqual(bug.bug_id, 1)

    def test_save_with_dict_in_custom_field(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        with self.assertRaisesMessage(ValueError,
                                      'Values stored in custom_fields cannot be tuples, lists, dictionaries'):
            Bug.objects.create(tracker=tracker, custom_fields={"field": {"toto": "gaga"}})

    def test_save_with_list_in_custom_field(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        with self.assertRaisesMessage(ValueError,
                                      'Values stored in custom_fields cannot be tuples, lists, dictionaries'):
            Bug.objects.create(tracker=tracker, custom_fields={"field": [0, 1, 2, 3]})

    def test_save_with_tuples_in_custom_field(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        with self.assertRaisesMessage(ValueError,
                                      'Values stored in custom_fields cannot be tuples, lists, dictionaries'):
            Bug.objects.create(tracker=tracker, custom_fields={"field": (0, 1, 2, 3)})

    def test_update_from_dict(self):
        upd_dict = {'severity': 'High', 'priority': 'Medium', 'non-existant': 42,
                    'id': 42, 'bug_id': 42, 'tracker_id': 42, 'tracker': 42,
                    'parent_id': 42, 'parent': 42}
        self.bug.update_from_dict(upd_dict)

        self.assertEqual(self.bug.severity, 'High')
        self.assertEqual(self.bug.priority, 'Medium')
        self.assertNotIn(42, self.bug.__dict__.values())

    def test_str(self):
        self.assertEqual(str(self.bug), "fdo#1234 - random title")


class TestReplicationScript(TestCase):
    def test_str(self):
        tracker = BugTracker.objects.create(name="Freedesktop.org", short_name="fdo", separator="#", public=False)
        tracker2 = BugTracker.objects.create(name="JIRA", short_name="jira", separator="#", public=False)
        rep_script = ReplicationScript.objects.create(name="My Script", source_tracker=tracker,
                                                      destination_tracker=tracker2)
        self.assertEqual(str(rep_script), "<replication script 'My Script'>")

    def test_script_validator(self):
        val_script = "def foo(): pass"
        res = script_validator(val_script)
        self.assertEqual(res, val_script)

    def test_script_validator_error(self):
        inval_script = "def foo("
        with self.assertRaises(ValidationError):
            script_validator(inval_script)


class TestBugComment(TestCase):
    def test_str(self):
        account = BugTrackerAccount(person=Person(full_name="John Doe", email="john.doe@isp.earth"))

        tracker = BugTracker(name="Freedesktop.org", short_name="fdo", separator="#")
        bug = Bug(tracker=tracker, bug_id="1234", title="random title",
                  created=timezone.now() - datetime.timedelta(days=4))

        comment = BugComment(bug=bug, account=account)
        self.assertEqual(str(comment), "{}'s comment by {}".format(bug, account))


class TestBuild(TestCase):
    def setUp(self):
        self.component = Component.objects.create(name='component', public=True)

    def test_url(self):
        self.assertEqual(Build(component=self.component, name='build').url,
                         '')
        self.assertEqual(Build(component=self.component, name='build', version='version').url,
                         'version')
        self.assertEqual(Build(component=self.component, name='build', repo='git://repo.com/repo',
                               version='version').url,
                         'version @ git://repo.com/repo')
        self.assertEqual(Build(component=self.component, name='build', upstream_url='https://repo.com/commit/version',
                               repo='git://repo.com/repo', version='version').url,
                         'https://repo.com/commit/version')


class VettableObjectMixin:
    def setUpVettableObject(self, vettable_object):
        self.vettable_object = vettable_object

    def test_vet(self):
        self.assertFalse(self.vettable_object.vetted)
        self.vettable_object.vet()
        self.assertTrue(self.vettable_object.vetted)

        self.assertRaisesMessage(ValueError, 'The object is already vetted',
                                 self.vettable_object.vet)

    def test_suppress(self):
        self.vettable_object.vetted_on = timezone.now()

        self.assertTrue(self.vettable_object.vetted)
        self.vettable_object.suppress()
        self.assertFalse(self.vettable_object.vetted)

        self.assertRaisesMessage(ValueError, 'The object is already suppressed',
                                 self.vettable_object.suppress)


class TestTests(TestCase, VettableObjectMixin):
    def setUp(self):
        self.ts = TestSuite.objects.create(name="testsuite", description="nothing", public=True)
        self.test = Test.objects.create(testsuite=self.ts, name="test", public=True)

        self.setUpVettableObject(self.test)

    def test__str__(self):
        self.assertEqual(str(self.test), "{}: {}".format(self.ts.name, self.test.name))

    @patch('CIResults.models.IssueFilterAssociated.objects.filter')
    def test_in_active_ifas(self, mock_filter):
        self.test.in_active_ifas
        mock_filter.assert_called_with(deleted_on=None, filter__tests__in=[self.test])

    @patch.object(Test, 'in_active_ifas', [MagicMock(), MagicMock()])
    def test_rename_public_test_to_existing_private_test(self):
        self.test.vetted_on = timezone.now()
        self.test.first_runconfig = RunConfig(name="test_runcfg")

        new_test = Test.objects.create(testsuite=self.ts, name="test2", public=False, vetted_on=timezone.now(),
                                       first_runconfig=RunConfig.objects.create(name="test2_runcfg", temporary=False))

        self.test.rename(new_test.name)

        # Fetch again the new test and check it got updated
        new_test = Test.objects.get(name="test2")
        self.assertTrue(new_test.public)
        self.assertEqual(new_test.testsuite, self.test.testsuite)
        self.assertEqual(new_test.first_runconfig.name, "test2_runcfg")  # This field should not have changed
        self.assertEqual(new_test.vetted_on, self.test.vetted_on)

        self.assertEqual(len(self.test.in_active_ifas), 2)
        for ifa in self.test.in_active_ifas:
            ifa.filter.tests.add.assert_called_with(new_test)

    @patch.object(Test, 'in_active_ifas', [MagicMock(), MagicMock()])
    def test_rename_test_to_new_bug(self):
        self.test.public = False
        self.test.vetted_on = timezone.now()
        self.test.first_runconfig = RunConfig(name="test_runcfg")

        self.test.rename('test2')

        # Fetchthe new test and check the fields got copied
        new_test = Test.objects.get(name="test2")
        self.assertFalse(new_test.public)
        self.assertEqual(new_test.testsuite, self.ts)
        self.assertEqual(new_test.first_runconfig, None)
        self.assertEqual(new_test.vetted_on, self.test.vetted_on)

        self.assertEqual(len(self.test.in_active_ifas), 2)
        for ifa in self.test.in_active_ifas:
            ifa.filter.tests.add.assert_called_with(new_test)


class MachineTagTests(TestCase):
    def test_machines(self):
        tag = MachineTag.objects.create(name='tag', public=True)
        machine1 = Machine.objects.create(name='machine1', public=True)
        machine1.tags.add(tag)

        machine2 = Machine.objects.create(name='machine2', public=True)
        machine2.tags.add(tag)

        Machine.objects.create(name='machine3', public=True)

        self.assertEqual(tag.machines, [machine1, machine2])

    def test_str(self):
        self.assertEqual(str(MachineTag(name='tag')), "tag")


class MachineTests(TestCase, VettableObjectMixin):
    def setUp(self):
        self.machine = Machine.objects.create(name="machine", public=True)
        self.setUpVettableObject(self.machine)

    @patch('CIResults.models.Machine.tags')
    def test_tags_cached(self, tags_cached_mocked):
        self.assertEqual(self.machine.tags_cached, tags_cached_mocked.all.return_value)

    def test__str__(self):
        self.assertEqual(str(self.machine), self.machine.name)


class RunConfigTests(TestCase):
    def setUp(self):
        self.testsuite = TestSuite.objects.create(name="testsuite", public=True)
        self.s_pass = TextStatus.objects.create(name="pass", testsuite=self.testsuite)
        self.s_fail = TextStatus.objects.create(name="fail", testsuite=self.testsuite)
        self.s_broken = TextStatus.objects.create(name="broken", testsuite=self.testsuite)
        self.testsuite.acceptable_statuses.add(self.s_pass)

        self.runconfig = RunConfig.objects.create(name="run", temporary=True)

    def test_public_no_tags(self):
        self.assertTrue(self.runconfig.public)

    def test_public_all_public(self):
        public_tag1 = RunConfigTag.objects.create(name="public_tag1", public=True)
        public_tag2 = RunConfigTag.objects.create(name="public_tag2", public=True)

        self.runconfig.tags.add(public_tag1)
        self.runconfig.tags.add(public_tag2)
        self.assertTrue(self.runconfig.public)

    def test_public_when_one_tag_is_private(self):
        public_tag1 = RunConfigTag.objects.create(name="public_tag1", public=True)
        public_tag2 = RunConfigTag.objects.create(name="public_tag2", public=True)
        private_tag = RunConfigTag.objects.create(name="private_tag", public=False)

        self.runconfig.tags.add(public_tag1)
        self.runconfig.tags.add(public_tag2)
        self.runconfig.tags.add(private_tag)
        self.assertFalse(self.runconfig.public)

    def test_update_statistics(self):
        self.machine = Machine.objects.create(name="machine", public=True)
        self.ts_run = TestsuiteRun.objects.create(testsuite=self.testsuite, runconfig=self.runconfig,
                                                  machine=self.machine, run_id=0,
                                                  start=timezone.now(), duration=datetime.timedelta(hours=1))
        self.tests = []
        self.testresults = []
        for i in range(4):
            self.tests.append(Test.objects.create(name="test{}".format(i),
                                                  testsuite=self.testsuite,
                                                  public=True))

            status = self.s_pass if i < 2 else self.s_fail
            self.testresults.append(TestResult.objects.create(test=self.tests[i],
                                                              ts_run=self.ts_run,
                                                              status=status,
                                                              start=timezone.now(),
                                                              duration=datetime.timedelta(seconds=3)))

        self.issue = Issue.objects.create(description="Issue", filer="me@me.de")

        # Create a filter that should not cover anything, and other that cover
        # and match a different count
        self.filters = []
        f = IssueFilter.objects.create(description="Covers nothing")
        f.statuses.add(self.s_broken)
        for i in range(len(self.tests)):
            f = IssueFilter.objects.create(description="Filter{}".format(i))
            f.tests.add(self.tests[i])
            if i + 1 < len(self.tests):
                f.tests.add(self.tests[i + 1])
            IssueFilterAssociated.objects.create(filter=f, issue=self.issue)

        # Try computing statistics as a temporary run first
        stats = self.runconfig.update_statistics()
        self.assertEqual(len(stats), 0)

        # Now check if the stastistics match with a non-temporary run
        self.runconfig.temporary = False
        stats = self.runconfig.update_statistics()
        stats.sort(key=lambda x: x.filter.id)

        expected_results = [(0, 2), (1, 2), (2, 2), (1, 1)]
        for i, stat in enumerate(stats):
            self.assertEqual(stat.matched_count, expected_results[i][0])
            self.assertEqual(stat.covered_count, expected_results[i][1])

    # TODO: Test runcfg_history and runcfg_history_offset


class TestSuiteTests(TestCase):
    def setUp(self):
        self.testsuite = TestSuite(name="testsuite1", public=True)
        self.testsuite.save()

    def test_str(self):
        self.assertEqual(str(self.testsuite), "testsuite1")

    def test_is_failure(self):
        statuses = []
        for i in range(4):
            status = TextStatus(name="status{}".format(i),
                                testsuite=self.testsuite)
            status.save()
            statuses.append(status)

        self.testsuite.acceptable_statuses.add(statuses[2])
        self.testsuite.acceptable_statuses.add(statuses[3])

        for i in range(0, 2):
            self.assertTrue(self.testsuite.is_failure(statuses[i]))
        for i in range(2, 4):
            self.assertFalse(self.testsuite.is_failure(statuses[i]))


class IssueTests(TestCase):
    Model = Issue

    def setUp(self):
        self.user = get_user_model().objects.create(username='blabla')

        created_on = timezone.now() - datetime.timedelta(seconds=1)
        self.issue = Issue.objects.create(description="blabla",
                                          filer="test@test.de",
                                          added_on=timezone.now())

        self.filters = []
        self.filtersAssoc = []
        for i in range(4):
            # Create the a filter and add it to the issue
            filter = IssueFilter.objects.create(description="Filter {}".format(i),
                                                added_on=created_on)
            self.filters.append(filter)

            # Create the association between the filter and the issue
            # WARNING: if the index is 2, close it immediately
            deleted_on = None if i != 2 else timezone.now()
            assoc = IssueFilterAssociated.objects.create(filter=filter, issue=self.issue,
                                                         added_on=created_on, added_by=self.user,
                                                         deleted_on=deleted_on)
            self.filtersAssoc.append(assoc)

        # Create multiple runconfigs
        self.runconfigs = []
        for i in range(5):
            r = RunConfig.objects.create(name="Runconfig {}".format(i),
                                         temporary=False,
                                         added_on=created_on + (i - 2) * datetime.timedelta(seconds=1))
            self.runconfigs.append(r)

            for filter in self.filters:
                RunFilterStatistic.objects.create(runconfig=r, filter=filter,
                                                  covered_count=0 if i < 2 else 10,
                                                  matched_count=0 if i < 3 else 3)

        self.issue.update_statistics()

    def test_active_filters(self):
        # Check that all the current filters are here (excluding the filter2
        # because it is got deleted already
        filters = set([self.filtersAssoc[0], self.filtersAssoc[1], self.filtersAssoc[3]])
        self.assertEqual(filters, set(self.issue.active_filters))

        # Now check that archiving does not change the result
        self.issue.archive(self.user)
        self.assertEqual(filters, set(self.issue.active_filters))

    def test_runconfigs_covered(self):
        runconfigs = set([self.runconfigs[2], self.runconfigs[3], self.runconfigs[4]])
        self.assertEqual(runconfigs, self.issue.runconfigs_covered)

        # Now check that archiving does not change the result
        self.issue.archive(self.user)
        self.assertEqual(runconfigs, self.issue.runconfigs_covered)

    def test_runconfigs_affected(self):
        runconfigs = set([self.runconfigs[3], self.runconfigs[4]])
        self.assertEqual(runconfigs, self.issue.runconfigs_affected)

        # Now check that archiving does not change the result
        self.issue.archive(self.user)
        self.assertEqual(runconfigs, self.issue.runconfigs_affected)

    def test_last_seen(self):
        self.assertEqual(self.issue.last_seen, self.runconfigs[-1].added_on)

    def test_failure_rate(self):
        failure_rate = self.issue.failure_rate
        self.assertAlmostEqual(2 / 3, failure_rate.rate)
        self.assertEqual("2 / 3 runs (66.7%)", str(failure_rate))

    def test_matches(self):
        # Intercept the calls to CIResults.models.IssueFilter.matches
        patcher_api_call = patch('CIResults.models.IssueFilter.matches')
        mock_matches = patcher_api_call.start()
        mock_matches.return_value = False
        self.addCleanup(patcher_api_call.stop)

        self.issue.matches(None)
        self.assertEqual(mock_matches.call_count, 3)

        # Now archive the issue, and check matches returns False without
        # calling $filter.matches()
        mock_matches.reset_mock()
        self.issue.archive(self.user)
        self.assertFalse(self.issue.matches(None))
        self.assertEqual(mock_matches.call_count, 0)

    def __count_active_filters__(self):
        count = 0
        for e in IssueFilterAssociated.objects.filter(issue=self.issue):
            if e.deleted_on is None:
                count += 1
            else:
                self.assertLess(timezone.now() - e.deleted_on,
                                datetime.timedelta(seconds=1),
                                "The deleted_on time is incorrect")
            self.assertEqual(e.added_by, self.user)
        return count

    def __check_comment_posted(self, comment_on_all_bugs_mock, substring):
        comment_on_all_bugs_mock.assert_called_once()
        args, kwargs = comment_on_all_bugs_mock.call_args_list[0]
        self.assertIn(substring, args[0])

    @patch('CIResults.models.Issue.comment_on_all_bugs')
    def test_archive(self, comment_on_all_bugs_mock):
        # Check the default state
        self.assertFalse(self.issue.archived)
        self.assertEqual(self.issue.archived_on, None)
        self.assertEqual(self.__count_active_filters__(), 3)

        # Archive the issue
        self.issue.archive(self.user)

        # Check that the filters' association has been updated
        self.assertEqual(self.__count_active_filters__(), 0)

        # Check that archived_on has been updated
        self.assertTrue(self.issue.archived)
        self.assertNotEqual(self.issue.archived_on, None)
        self.assertLess(timezone.now() - self.issue.archived_on,
                        datetime.timedelta(seconds=1),
                        "The archived_on time is incorrect")
        self.assertEqual(self.issue.archived_by, self.user)

        # Check that we posted a comment on the bugs associated
        self.__check_comment_posted(comment_on_all_bugs_mock, "archived")

        # Check that archiving the issue again generates an error
        self.assertRaisesMessage(ValueError, "The issue is already archived",
                                 self.issue.archive, self.user)

    @patch('CIResults.models.Issue.comment_on_all_bugs')
    def test_restore(self, comment_on_all_bugs_mock):
        # Archive the issue
        # TODO: Load a fixture with an archived issue and an unknown failure
        self.issue.archived_on = timezone.now()

        # Restore the issue before checking the restoration process
        self.issue.restore()

        # Check that the filters' association has been updated
        self.assertEqual(self.__count_active_filters__(), 3)

        # Check that archived_on has been updated
        self.assertFalse(self.issue.archived)
        self.assertEqual(self.issue.archived_on, None)
        self.assertEqual(self.issue.archived_by, None)

        # TODO: Make sure the unknown failure became a known failure, associated
        # to this issue. To be done after migrating to the fixture

        # Check that we posted a comment on the bugs associated
        self.__check_comment_posted(comment_on_all_bugs_mock, "restored")

        # Check that restoring the issue again generates an error
        self.assertRaisesMessage(ValueError, "The issue is not currently archived",
                                 self.issue.restore)

    def test_set_bugs(self):
        # Create some bugs
        bugs = []
        tracker = BugTracker.objects.create(name="Tracker", tracker_type="jira_untracked", public=True)
        for i in range(5):
            bugs.append(Bug(tracker=tracker, bug_id=str(i), title="bug {}".format(i)))

        # Add some bugs
        self.issue.set_bugs(bugs[2:4])
        self.assertEqual(set(self.issue.bugs_cached), set(bugs[2:4]))

        # Now try to update the bugs
        self.issue.set_bugs(bugs[0:3])
        self.assertEqual(set(self.issue.bugs_cached), set(bugs[0:3]))

        # Archive the issue, and see if updating the bug generates an assert
        self.issue.archive(self.user)
        self.assertRaisesMessage(ValueError, "The issue is archived, and thus read-only",
                                 self.issue.set_bugs, [])

    @patch('CIResults.models.IssueFilterAssociated.objects.create')
    @patch('CIResults.models.UnknownFailure.objects.prefetch_related',
           return_value=MagicMock(all=MagicMock(return_value=[MagicMock(spec=UnknownFailure),
                                                              MagicMock(spec=UnknownFailure),
                                                              MagicMock(spec=UnknownFailure)])))
    @patch('CIResults.models.KnownFailure.objects.create')
    def test___filter_add__(self, knownfailure_mocked, queryset_mocked, ifa_created_mocked):
        # Finish mocking the failures
        failures = queryset_mocked.return_value.all.return_value
        failures[0].result.ts_run.reported_on = timezone.make_aware(datetime.datetime.fromtimestamp(0),
                                                                    timezone.get_default_timezone())
        failures[2].result.ts_run.reported_on = timezone.make_aware(datetime.datetime.fromtimestamp(10),
                                                                    timezone.get_default_timezone())

        # Make a filter, and add it to the issue
        filter = MagicMock(spec=IssueFilter)
        filter.matches = MagicMock(side_effect=[True, False, True])

        self.issue.__filter_add__(filter, self.user)

        # Check that a IssueFilterAssociated is created
        ifa_created_mocked.assert_called_with(filter=filter, issue=self.issue, added_by=self.user)

        # Check that the filter's match function is called on all the UnknownFailure
        self.assertEqual(filter.matches.call_count, 3)

        # Check that all the matching resulted in a KnownFailure, and that the UnknownFailure got deleted
        self.assertEqual(knownfailure_mocked.call_count, 2)
        failures[0].delete.assert_called_with()
        failures[1].delete.assert_not_called()
        failures[2].delete.assert_called_with()

        # Check that the manually_associated_on and filing_delay fields have been set correctly
        self.assertLess(timezone.now() - knownfailure_mocked.call_args_list[0][1]['manually_associated_on'],
                        datetime.timedelta(seconds=1))
        self.assertLess(timezone.now() - knownfailure_mocked.call_args_list[1][1]['manually_associated_on'],
                        datetime.timedelta(seconds=1))

        assoc_time = knownfailure_mocked.call_args_list[0][1]['manually_associated_on']
        self.assertEqual(knownfailure_mocked.call_args_list[0][1]['filing_delay'],
                         assoc_time - failures[0].result.ts_run.reported_on)
        self.assertEqual(knownfailure_mocked.call_args_list[1][1]['filing_delay'],
                         assoc_time - failures[2].result.ts_run.reported_on)

    def test_replace_filter(self):
        old_filter = IssueFilter.objects.create(description="old filter")
        new_filter = IssueFilter.objects.create(description="new filter")

        # Add the old filter once before deleting it
        self.issue.set_filters([old_filter], self.user)
        self.issue.set_filters([], self.user)

        self.assertEqual(IssueFilterAssociated.objects.exclude(deleted_on=None).filter(filter=old_filter).count(), 1)

        # Re-add the old filter, then replace it with the new one
        self.issue.set_filters([old_filter], self.user)
        self.issue.replace_filter(old_filter, new_filter, self.user)

        self.assertEqual(IssueFilterAssociated.objects.exclude(deleted_on=None).filter(filter=old_filter).count(), 2)
        self.assertEqual(IssueFilterAssociated.objects.filter(deleted_on=None, filter=new_filter).count(), 1)

        # Archive the issue, and see if replacing the filter generates an assert
        self.issue.archive(self.user)
        self.assertRaisesMessage(ValueError, "The issue is archived, and thus read-only",
                                 self.issue.replace_filter, new_filter, old_filter, self.user)

    def test_set_filters(self):
        # Check the current amount of filters
        self.assertEqual(set([e.filter for e in self.issue.active_filters]),
                         set([self.filters[0], self.filters[1], self.filters[3]]))
        self.assertEqual(set([e.filter for e in self.issue.all_filters]), set(self.filters))

        # Now try to update the filters
        self.issue.set_filters(self.filters[0:3], self.user)
        self.assertEqual(set([e.filter for e in self.issue.active_filters]), set(self.filters[0:3]))
        self.assertEqual(set([e.filter for e in self.issue.all_filters]), set(self.filters))

        # Check that the deleted_on field has been updated for the filter 3
        expected_len = [1, 1, 2, 1]
        expected_deleted_none = [True, True, False, False]
        for i in range(len(self.filters)):
            db_assocs = IssueFilterAssociated.objects.filter(filter=self.filters[i], issue=self.issue)
            self.assertEqual(len(db_assocs), expected_len[i], i)
            if len(db_assocs) > 0:
                self.assertEqual(db_assocs[0].deleted_on is None, expected_deleted_none[i], i)

        # Archive the issue, and see if updating the filter generates an assert
        self.issue.archive(self.user)
        self.assertRaisesMessage(ValueError, "The issue is archived, and thus read-only",
                                 self.issue.set_filters, [], self.user)

    def test_str(self):
        self.assertEqual(str(self.issue), "Issue: <empty>")

        tracker = BugTracker(name="Freedesktop.org", short_name="fdo",
                                  separator="#", public=True,
                                  url="https://bugs.freedesktop.org/",
                                  bug_base_url="https://bugs.freedesktop.org/show_bug.cgi?id=")
        tracker.save()
        bug1 = Bug.objects.create(tracker=tracker, bug_id="1234", title="random title")
        bug2 = Bug.objects.create(tracker=tracker, bug_id="1235", title="random title")

        self.issue.bugs.add(bug1)
        self.assertEqual(str(self.issue), "Issue: fdo#1234 - random title")

        self.issue.bugs.add(bug2)
        self.assertEqual(str(self.issue), "Issue: [fdo#1234, fdo#1235]")


class IssueFilterAssociatedTests(TestCase):
    def setUp(self):
        self.filter = IssueFilter.objects.create(description="Filter")
        self.issue = Issue.objects.create(filer="m@x.org")

    def test_delete(self):
        # Create an association and check that the field deleted_on is not set
        assoc = IssueFilterAssociated(filter=self.filter, issue=self.issue)
        self.assertEqual(assoc.deleted_on, None)
        self.assertEqual(assoc.id, None)

        # Delete the association, and verify that it actually got saved to the DB
        # by checking if the id has been set
        user = get_user_model().objects.create(username='blabla')
        assoc.delete(user, datetime.datetime.fromtimestamp(0, tz=pytz.utc))
        self.assertEqual(assoc.deleted_by, user)
        self.assertNotEqual(assoc.id, None)
        self.assertEqual(assoc.deleted_on, datetime.datetime.fromtimestamp(0, tz=pytz.utc))

        # Try deleting again with a new timestamp, and check that it did not change
        assoc.delete(datetime.datetime.fromtimestamp(1, tz=pytz.utc))
        self.assertEqual(assoc.deleted_on, datetime.datetime.fromtimestamp(0, tz=pytz.utc))

        # Test what happens when we omit the delete's now argument
        assoc = IssueFilterAssociated(filter=self.filter, issue=self.issue)
        assoc.delete(user)
        self.assertLess(abs(timezone.now() - assoc.deleted_on), datetime.timedelta(seconds=1))
        self.assertEqual(assoc.deleted_by, user)

    # TODO: Test all the statistics!


class TextStatusTests(TestCase, VettableObjectMixin):
    def setUp(self):
        self.testsuite = TestSuite.objects.create(name="testsuite", public=True)
        self.status = TextStatus.objects.create(testsuite=self.testsuite, name="status")
        self.setUpVettableObject(self.status)

        self.pass_status = TextStatus.objects.create(testsuite=self.testsuite, name="pass")
        self.testsuite.acceptable_statuses.add(self.pass_status)

        self.notrun = TextStatus.objects.create(testsuite=self.testsuite, name="notrun")
        self.testsuite.notrun_status = self.notrun

    def test_color__with_specified_color(self):
        self.assertEqual(TextStatus(color_hex="#123456").color, "#123456")

    def test_color__default(self):
        self.assertEqual(TextStatus().color, "#e9be2c")

    def test_is_failure(self):
        self.assertTrue(self.status.is_failure)
        self.assertFalse(self.pass_status.is_failure)

    def test_is_notrun(self):
        self.assertTrue(self.notrun.is_notrun)
        self.assertFalse(self.status.is_notrun)

    def test_actual_severity(self):
        self.assertEqual(TextStatus(severity=42).actual_severity, 42)

        self.assertEqual(self.notrun.actual_severity, 0)
        self.assertEqual(self.pass_status.actual_severity, 1)
        self.assertEqual(self.status.actual_severity, 2)

    def test_str(self):
        self.assertEqual(str(self.status), "testsuite: status")


class IssueFilterTests(TestCase):
    def setUp(self):
        self.tag = []
        self.runconfigs = []
        self.machine = []
        self.test = []
        self.status = []
        self.ts_run = []
        self.testresult = []

        self.testsuite = TestSuite(name="testsuite1", public=True)
        self.testsuite.save()

        # Create 4 instances of random objects
        for i in range(4):
            tag = RunConfigTag(public=True, name="tag{}".format(i))
            tag.save()
            self.tag.append(tag)

            machine = Machine(name="machine{}".format(i), public=True)
            machine.save()
            self.machine.append(machine)

            test = Test(name="test{}".format(i), testsuite=self.testsuite,
                        public=True)
            test.save()
            self.test.append(test)

            status = TextStatus(name="status{}".format(i), testsuite=self.testsuite)
            status.save()
            self.status.append(status)

        # Tell which results are acceptable for the testsuite
        self.testsuite.acceptable_statuses.add(self.status[1])
        self.testsuite.acceptable_statuses.add(self.status[2])

        # Create the runconfig and ts_runs
        for i in range(3):
            runconfig = RunConfig(name="runconfig{}".format(i), temporary=False)
            runconfig.save()

            # Add $i tags
            if i > 0:
                runconfig.tags.add(self.tag[i - 1])

            # Create a testsuite run for all machines
            for machine in self.machine:
                self.ts_run.append(TestsuiteRun(testsuite=self.testsuite, runconfig=runconfig,
                                                machine=machine, run_id=0))
            self.runconfigs.append(runconfig)

        # Create the test results
        for test in self.test:
            for ts_run in self.ts_run:
                for status in self.status:
                    self.testresult.append(TestResult(test=test, ts_run=ts_run,
                                                      status=status,
                                                      stdout="h\n{} stdout1234\nYoo!".format(status.name),
                                                      stderr="h\n{} stderr1234\nasdf".format(status.name),
                                                      dmesg="h\n{} dmesg1234\nqwer".format(status.name)))

        self.filter = IssueFilter()
        self.filter.save()

    def test_empty(self):
        for testresult in self.testresult:
            self.assertTrue(self.filter.covers(testresult))
            self.assertTrue(self.filter.matches(testresult))

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': ""}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), expected)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), expected)

    def test_runconfig_tag_only(self):
        self.filter.tags.add(self.tag[0])
        self.filter.tags.add(self.tag[2])

        filter_tags = set([self.tag[0].id, self.tag[2].id])
        for testresult in self.testresult:
            tags = set([t.id for t in testresult.ts_run.runconfig.tags.all()])
            should_match = not filter_tags.isdisjoint(tags)
            self.assertTrue(self.filter.covers(testresult) == should_match)
            self.assertTrue(self.filter.matches(testresult) == should_match)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': 'runconfig_tag IS IN ["tag0", "tag2"]'}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), expected)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), {'query': ""})

    def test_machine_and_machine_tags(self):
        tag1 = MachineTag.objects.create(name="Tag1", public=True)
        self.machine[0].tags.add(tag1)
        self.machine[2].tags.add(tag1)

        self.filter.machine_tags.add(tag1)

        for testresult in self.testresult:
            machine = testresult.ts_run.machine
            should_match = (machine == self.machine[0] or machine == self.machine[2])
            self.assertTrue(self.filter.covers(testresult) == should_match)
            self.assertTrue(self.filter.matches(testresult) == should_match)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': '(machine_name IS IN ["machine0", "machine2"] OR machine_tag IS IN ["Tag1"])'}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), expected)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), {'query': ""})

    def test_machine_tag_only(self):
        # Test that if a user asks for a tag that contains no machines, we do not match anything
        tag1 = MachineTag.objects.create(name="Tag1", public=True)
        self.filter.machine_tags.add(tag1)

        for testresult in self.testresult:
            self.assertFalse(self.filter.covers(testresult))
            self.assertFalse(self.filter.matches(testresult))

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': 'machine_tag IS IN ["Tag1"]'}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), expected)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), {'query': ""})

    def test_machine_only(self):
        self.filter.machines.add(self.machine[0])
        self.filter.machines.add(self.machine[2])

        for testresult in self.testresult:
            machine = testresult.ts_run.machine
            should_match = (machine == self.machine[0] or machine == self.machine[2])
            self.assertTrue(self.filter.covers(testresult) == should_match)
            self.assertTrue(self.filter.matches(testresult) == should_match)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        machine_names = [mach.name for mach in self.filter.machines_cached]
        expected = {'query': 'machine_name IS IN ["{}", "{}"]'.format(machine_names[0], machine_names[1])}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), expected)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), {'query': ""})

    def test_test_only(self):
        # Create another status from another testsuite
        testsuite2 = TestSuite.objects.create(name="testsuite2", public=True)
        test_ts2 = Test.objects.create(name="test-ts2", testsuite=testsuite2, public=True)

        self.filter.tests.add(self.test[0], self.test[2], test_ts2)

        for testresult in self.testresult:
            test = testresult.test
            should_match = (test == self.test[0] or test == self.test[2])
            self.assertTrue(self.filter.covers(testresult) == should_match)
            self.assertTrue(self.filter.matches(testresult) == should_match)

        # Generate all the valid queries that could be generated
        query_opts = []
        q1 = '(testsuite_name = "testsuite1" AND test_name IS IN ["{}", "{}"])'
        q2 = '(testsuite_name = "testsuite2" AND test_name IS IN ["test-ts2"])'
        query_pattern = '({} OR {})'
        query_opts.append({'query': query_pattern.format(q1.format("test0", "test2"), q2)})
        query_opts.append({'query': query_pattern.format(q1.format("test2", "test0"), q2)})
        query_opts.append({'query': query_pattern.format(q2, q1.format("test2", "test0"))})
        query_opts.append({'query': query_pattern.format(q2, q1.format("test0", "test2"))})

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        self.assertIn(self.filter.to_user_query, query_opts)
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), {'query': ""})
        self.assertIn(self.filter._to_user_query(covers=True, matches=False), query_opts)

    def test_results_only(self):
        # Create another status from another testsuite
        testsuite2 = TestSuite.objects.create(name="testsuite2", public=True)
        status_ts2 = TextStatus.objects.create(name="status-ts2", testsuite=testsuite2)

        self.filter.statuses.add(self.status[0], self.status[2], status_ts2)

        for testresult in self.testresult:
            status = testresult.status
            should_match = (status == self.status[0] or status == self.status[2])
            self.assertTrue(self.filter.covers(testresult))
            self.assertTrue(self.filter.matches(testresult) == should_match, testresult)

        # Generate all the valid queries that could be generated
        query_opts = []
        q1 = '(testsuite_name = "testsuite1" AND status_name IS IN ["{}", "{}"])'
        q2 = '(testsuite_name = "testsuite2" AND status_name IS IN ["status-ts2"])'
        query_pattern = '({} OR {})'
        query_opts.append({'query': query_pattern.format(q1.format("status0", "status2"), q2)})
        query_opts.append({'query': query_pattern.format(q1.format("status2", "status0"), q2)})
        query_opts.append({'query': query_pattern.format(q2, q1.format("status2", "status0"))})
        query_opts.append({'query': query_pattern.format(q2, q1.format("status0", "status2"))})

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        self.assertIn(self.filter.to_user_query, query_opts)
        self.assertIn(self.filter._to_user_query(covers=False, matches=True), query_opts)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), {'query': ""})

    def test_stdout_only(self):
        self.filter.stdout_regex = r"result[12] stdout\d+"
        for testresult in self.testresult:
            stdout_line = testresult.stdout.split('\n')[1]
            should_match = (stdout_line == "result1 stdout1234" or stdout_line == "result2 stdout1234")
            self.assertTrue(self.filter.covers(testresult))
            self.assertTrue(self.filter.matches(testresult) == should_match, stdout_line)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': r"stdout ~= 'result[12] stdout\d+'"}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), {'query': ""})
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), expected)

    def test_stderr_only(self):
        self.filter.stderr_regex = r"result[12] stderr\d+"
        for testresult in self.testresult:
            stderr_line = testresult.stderr.split('\n')[1]
            should_match = (stderr_line == "result1 stderr1234" or stderr_line == "result2 stderr1234")
            self.assertTrue(self.filter.covers(testresult))
            self.assertTrue(self.filter.matches(testresult) == should_match, stderr_line)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': r"stderr ~= 'result[12] stderr\d+'"}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), {'query': ""})
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), expected)

    def test_dmesg_only(self):
        self.filter.dmesg_regex = r"result[12] dmesg\d+"
        for testresult in self.testresult:
            dmesg_line = testresult.dmesg.split('\n')[1]
            should_match = (dmesg_line == "result1 dmesg1234" or dmesg_line == "result2 dmesg1234")
            self.assertTrue(self.filter.covers(testresult))
            self.assertTrue(self.filter.matches(testresult) == should_match, dmesg_line)

        # Test the conversion to user filters
        TestResult.from_user_filters(**self.filter.to_user_query)
        expected = {'query': r"dmesg ~= 'result[12] dmesg\d+'"}
        self.assertEqual(self.filter.to_user_query, expected)
        self.assertEqual(self.filter._to_user_query(covers=True, matches=False), {'query': ""})
        self.assertEqual(self.filter._to_user_query(covers=False, matches=True), expected)

    def test_replace(self):
        user = get_user_model().objects.create(username='blabla')

        issues = []
        for i in range(3):
            issue = Issue.objects.create(description="")
            issue.set_filters([self.filter], user)
            issues.append(issue)
            if i == 1:
                issue.archive(user)

        new_filter = IssueFilter.objects.create(description="new filter")

        self.assertEqual(IssueFilterAssociated.objects.filter(filter=self.filter).count(), 3)
        self.assertEqual(IssueFilterAssociated.objects.filter(filter=self.filter, deleted_by=user).count(), 1)
        self.assertEqual(IssueFilterAssociated.objects.filter(deleted_on=None, filter=self.filter).count(), 2)
        self.assertEqual(IssueFilterAssociated.objects.filter(filter=new_filter).count(), 0)
        self.assertFalse(self.filter.hidden)

        self.filter.replace(new_filter, user)

        self.assertEqual(IssueFilterAssociated.objects.filter(filter=self.filter).count(), 3)
        self.assertEqual(IssueFilterAssociated.objects.filter(deleted_on=None, filter=self.filter).count(), 0)
        self.assertEqual(IssueFilterAssociated.objects.filter(filter=new_filter).count(), 2)
        self.assertEqual(IssueFilterAssociated.objects.filter(filter=self.filter, deleted_by=user).count(), 3)
        self.assertTrue(self.filter.hidden)

    # Verify that the all the machines specified by tags are listed when calling test_machines_cached
    def test_machines_cached(self):
        # Create a machine tag and tag machine1 with it
        tag1 = MachineTag.objects.create(name="TAG1", public=True)
        self.machine[1].tags.add(tag1)
        self.machine[3].tags.add(tag1)

        # Now add the tag1 to the list of machines tags, and machine 0 as a machine
        self.filter.machine_tags.add(tag1)
        self.filter.machines.add(self.machine[0])

        # Now check that the filter lists the machines 1 and 2 when calling test_machines_cached
        self.assertEqual(self.filter.machines_cached, set([self.machine[0], self.machine[1], self.machine[3]]))


class RateTests(TestCase):
    def test_rate(self):
        self.assertAlmostEqual(Rate('', 1, 2).rate, 0.5)
        self.assertEqual(Rate('', 0, 0).rate, 0)

    def test_str(self):
        self.assertEqual(str(Rate('tests', 1, 2)), '1 / 2 tests (50.0%)')


class KnownFailureTests(TestCase):
    def test_covered_runconfigs_since(self):
        # Create a list of runconfigs that will be used by the "runconfig_covered"
        runconfigs = list()
        for i in range(6):
            runconfigs.append(RunConfig.objects.create(name="Runconfig{}".format(i), temporary=True))

        # Create a failure linked to an Issue with all the runconfigs as covered, and the ifa's
        # covered runconfigs are all but the last one. Associate the runconfig index 1 as the runconfig
        # where the failure happened
        failure = KnownFailure()
        failure.result = TestResult(ts_run=TestsuiteRun(runconfig=runconfigs[1]))
        failure.matched_ifa = IssueFilterAssociated(issue=Issue())
        failure.matched_ifa.runconfigs_covered = runconfigs[:-1]
        failure.matched_ifa.issue.runconfigs_covered = runconfigs

        # Check that when the runconfig is not found, we return None
        self.assertEqual(KnownFailure._runconfig_index(runconfigs, RunConfig()), None)

        # Check that the number of runconfigs since the failure happened is
        self.assertEqual(failure.covered_runconfigs_since_for_issue, 4)   # len(issue.runconfig_covered) - 2
        self.assertEqual(failure.covered_runconfigs_since_for_filter, 3)  # len(ifa.runconfig_covered) - 2


class UnknownFailureTests(TestCase):
    @patch('CIResults.models.UnknownFailure.matched_archived_ifas')
    def test_matched_archived_ifas_cached(self, matched_archived_ifas_mocked):
        self.assertEqual(UnknownFailure().matched_archived_ifas_cached, matched_archived_ifas_mocked.all.return_value)

    def test_matched_issues(self):
        failure = UnknownFailure()
        failure.matched_archived_ifas_cached = [MagicMock(spec=IssueFilterAssociated),
                                                MagicMock(spec=IssueFilterAssociated)]
        self.assertEqual(failure.matched_issues,
                         set([e.issue for e in failure.matched_archived_ifas_cached]))

    @patch('CIResults.models.UnknownFailure.result')
    def test_str(self, results_mocked):
        failure = UnknownFailure()
        self.assertEqual(str(failure), str(failure.result))


class RunFilterStatisticTests(TestCase):
    def test_str(self):
        f = IssueFilter(description="My filter")
        runconfig = RunConfig(name='RunConfig')

        self.assertEqual(str(RunFilterStatistic(runconfig=runconfig, filter=f, covered_count=0, matched_count=0)),
                         'My filter on RunConfig: match rate 0/0 (0.00%)')
        self.assertEqual(str(RunFilterStatistic(runconfig=runconfig, filter=f, covered_count=10, matched_count=5)),
                         'My filter on RunConfig: match rate 5/10 (50.00%)')
