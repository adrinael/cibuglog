from unittest.mock import patch, MagicMock
from rest_framework.test import APIRequestFactory, APITestCase
from django.conf import settings
from django.db import transaction
from django.test import TestCase
from django.http.response import Http404
from django.urls import reverse

from CIResults.models import Test, Machine, RunConfigTag, TestSuite, RunConfig
from CIResults.models import TextStatus, IssueFilter, MachineTag, Issue, Bug, BugTracker
from CIResults.rest_views import CustomPagination, IssueFilterViewSet, RunConfigViewSet
from CIResults.tests.test_views import create_user_and_log_in

from shortener.models import Shortener


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class CustomPaginationTests(TestCase):
    def get_page_size(self, query_params, page_size_query_param='page_size', max_page_size=None):
        request = MagicMock(query_params=query_params)

        pagination = CustomPagination()
        pagination.page_size_query_param = page_size_query_param
        pagination.max_page_size = max_page_size

        return pagination.get_page_size(request)

    def test_default_page_size(self):
        self.assertEqual(self.get_page_size({}), CustomPagination.page_size)

    def test_default_page_size_without_page_size_field(self):
        self.assertEqual(self.get_page_size({}, page_size_query_param=None), CustomPagination.page_size)

    def test_invalid_page_size(self):
        self.assertEqual(self.get_page_size({'page_size': 'toto'}), CustomPagination.page_size)

    def test_negative_page_size(self):
        self.assertEqual(self.get_page_size({CustomPagination.page_size_query_param: -0}), None)

    def test_page_size_too_big(self):
        self.assertEqual(self.get_page_size({'page_size': '1000'}, max_page_size=10), 10)

    def test_page_size_big_but_no_limits(self):
        self.assertEqual(self.get_page_size({'page_size': '1000'}, max_page_size=None), 1000)

    def test_acceptable_page_size(self):
        self.assertEqual(self.get_page_size({'page_size2': '42'}, page_size_query_param='page_size2'), 42)


class IssueFilterTests(APITestCase):
    maxDiff = None

    def setUp(self):
        self.view = IssueFilterViewSet()
        self.user = None

    def __post__(self, body_dict, logged_in=True, permissions=['add_issuefilter']):
        if logged_in:
            self.user = create_user_and_log_in(self.client, permissions=permissions)

        return self.client.post(reverse('api:issuefilter-list'), body_dict, format='json')

    def test__get_or_None__empty_field(self):
        errors = []
        self.assertIsNone(self.view.__get_or_None__(Machine, 'field', {'field': ''}, errors))
        self.assertEqual(errors, [])

    def test__get_or_None__invalid_id(self):
        errors = []
        self.assertIsNone(self.view.__get_or_None__(Machine, 'field', {'field': '12'}, errors))
        self.assertEqual(errors, ["The object referenced by 'field' does not exist"])

    def test_create_empty(self):
        response = self.__post__({})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'description' cannot be empty"])

    def test_invalid_regexps(self):
        response = self.__post__({"description": "Minimal IssueFilter",
                                  "stdout_regex": '(', "stderr_regex": '(',
                                  "dmesg_regex": '('})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'stdout_regex' does not contain a valid regular expression",
                                         "The field 'stderr_regex' does not contain a valid regular expression",
                                         "The field 'dmesg_regex' does not contain a valid regular expression"])

    def test_create_minimal__unauthenticated(self):
        response = self.__post__({"description": "Minimal IssueFilter"}, logged_in=False)
        self.assertEqual(response.status_code, 401)

    # FIXME: This is not currently working
    # def test_create_minimal__invalid_permissions(self):
        # response = self.__post__({"description": "Minimal IssueFilter"}, logged_in=True, permissions=[])
        # self.assertEqual(response.status_code, 403)

    def test_create_minimal(self):
        response = self.__post__({"description": "Minimal IssueFilter"})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'Minimal IssueFilter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'Minimal IssueFilter'})

    def test_create_invalid(self):
        response = self.__post__({
                "description": "Invalid tags, machines, tests, and statuses",
                'tags': [1], 'machines': [2], 'tests': [3], 'statuses': [4]
        })
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ['At least one tag does not exist',
                                         'At least one machine does not exist',
                                         'At least one test does not exist',
                                         'At least one status does not exist'])

    def test_create_complete(self):
        # Create some objects before referencing them
        ts = TestSuite.objects.create(name="ts", description="", url="", public=True)
        for i in range(1, 6):
            RunConfigTag.objects.create(id=i, name="tag{}".format(i), description="", url="", public=True)
            Machine.objects.create(id=i, name="machine{}".format(i), public=True)
            Test.objects.create(id=i, name="test{}".format(i), testsuite=ts, public=True)
            TextStatus.objects.create(id=i, testsuite=ts, name="status{}".format(i))
            MachineTag.objects.create(id=i, name="TAG{}".format(i), public=True)

        # Make the request and check that we get the expected output
        with transaction.atomic():
            response = self.__post__({
                "description": "Minimal IssueFilter",
                'tags': [1, 2], 'machine_tags': [1, 5], 'machines': [2, 3], 'tests': [3, 4], 'statuses': [4, 5],
                'stdout_regex': 'stdout', 'stderr_regex': 'stderr', 'dmesg_regex': 'dmesg'
                })

        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(dict(response.data),
                         {'id': response.data['id'], 'description': 'Minimal IssueFilter',
                          'tags': [
                              {'id': 1, 'description': '', 'url': '', 'public': True, '__str__': 'tag1'},
                              {'id': 2, 'description': '', 'url': '', 'public': True, '__str__': 'tag2'}
                          ],
                          'machine_tags': [
                              {'id': 1, 'name': 'TAG1', 'public': True},
                              {'id': 5, 'name': 'TAG5', 'public': True},
                          ],
                          'machines': [
                              {'id': 2, 'vetted_on': None, 'public': True, '__str__': 'machine2'},
                              {'id': 3, 'vetted_on': None, 'public': True, '__str__': 'machine3'},
                              ],
                          'tests': [
                              {'id': 3, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'public': True,
                               'vetted_on': None, 'first_runconfig': None, 'name': 'test3', '__str__': 'ts: test3'},
                              {'id': 4, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'public': True,
                               'vetted_on': None, 'first_runconfig': None, 'name': 'test4', '__str__': 'ts: test4'},
                          ],
                          'statuses': [
                              {'id': 4, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'name': 'status4',
                               '__str__': 'ts: status4'},
                              {'id': 5, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'name': 'status5',
                               '__str__': 'ts: status5'},
                          ],
                          'stdout_regex': 'stdout', 'stderr_regex': 'stderr',
                          'dmesg_regex': 'dmesg', '__str__': 'Minimal IssueFilter'})

    def test_edit_invalid(self):
        response = self.__post__({"description": "new filter",
                                  "edit_filter": "a", "edit_issue": "b"})
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ["The field 'edit_filter' needs to be an integer",
                                         "The field 'edit_issue' needs to be an integer"])

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_all_issues(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="old filter")

        with transaction.atomic():
            response = self.__post__({"description": "new filter", "edit_filter": filter.id})
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=response.data['id'])
        mock_replace_filter.assert_not_called()
        mock_filter_replace.assert_called_once_with(new_filter, self.user)
        self.assertEqual(mock_filter_replace.call_args_list[0][0][0].id, response.data['id'])

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_one_issue(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="desc")
        issue = Issue.objects.create(description="issue")

        with transaction.atomic():
            response = self.__post__({"description": "new filter",
                                      "edit_filter": "{}".format(filter.id),
                                      "edit_issue": issue.id})

        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=response.data['id'])
        mock_filter_replace.assert_not_called()
        mock_replace_filter.assert_called_once_with(filter, new_filter, self.user)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][0].id, filter.id)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][1].id, response.data['id'])


class RunConfigTests(TestCase):
    def setUp(self):
        self.view = RunConfigViewSet()
        self.arf = APIRequestFactory()

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_id(self, runcfg_get_mocked):
        self.assertEqual(self.view._get_runcfg('1'), runcfg_get_mocked.return_value)
        runcfg_get_mocked.assert_called_once_with(pk=1)

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_invalid_name(self, runcfg_get_mocked):
        self.assertRaises(Http404, self.view._get_runcfg, 'missing')
        runcfg_get_mocked.assert_not_called()

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_valid_name(self, runcfg_get_mocked):
        rc = RunConfig.objects.create(name='valid', temporary=True)
        self.assertEqual(self.view._get_runcfg('valid').pk, rc.id)
        runcfg_get_mocked.assert_not_called()

    def test_known_failures(self):
        # TODO: Revisit the test whenever we start generating fixtures
        RunConfig.objects.create(name='valid', temporary=True)
        response = self.view.known_failures(self.arf.get('/', {}, format='json'),
                                            pk='valid')
        self.assertEqual(response.data, [])

    @patch('CIResults.serializers.RunConfigDiffSerializer.data')
    @patch('CIResults.models.RunConfig.compare')
    def test_compare(self, compare_mocked, serializer_mocked):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        runcfg2 = RunConfig.objects.create(name='runcfg2', temporary=True)
        response = self.view.compare(self.arf.get('/', {'to': 'runcfg2'}, format='json'),
                                     pk='runcfg1')
        compare_mocked.assert_called_once_with(runcfg2, no_compress=False)
        self.assertEqual(response.data, serializer_mocked)

    @patch('CIResults.models.RunConfig.compare')
    def test_compare__no_compress(self, compare_mocked):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        runcfg2 = RunConfig.objects.create(name='runcfg2', temporary=True)
        self.view.compare(self.arf.get('/', {'to': 'runcfg2', 'no_compress': '1'}, format='json'),
                          pk='runcfg1')
        compare_mocked.assert_called_once_with(runcfg2, no_compress=True)

    def test_compare__only_summary(self):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        RunConfig.objects.create(name='runcfg2', temporary=True)
        response = self.view.compare(self.arf.get('/', {'to': 'runcfg2', 'summary': ''}, format='json'),
                                     pk='runcfg1')
        self.assertIn('CI Bug Log', response.content.decode())


class BugViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    @classmethod
    def setUpTestData(cls):
        cls.bt1 = BugTracker.objects.create(name='Test Suite 1', short_name='ts1', public=True)
        cls.bt2 = BugTracker.objects.create(name='Test Suite 2', short_name='ts2', public=True)

        cls.bugs = dict()
        for bt in (cls.bt1, cls.bt2):
            cls.bugs[bt] = []
            for i in range(2):
                cls.bugs[bt].append(Bug.objects.create(tracker=bt, bug_id='b_'+str(i)))

    def test_retrieving_by_tracker_id(self):
        tracker = self.bt2
        bug = self.bugs[self.bt2][1]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.id, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)

    def test_retrieving_by_tracker_name(self):
        tracker = self.bt1
        bug = self.bugs[self.bt2][0]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.name, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)

    def test_retrieving_by_tracker_short_name(self):
        tracker = self.bt2
        bug = self.bugs[self.bt2][0]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.short_name, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)


class ShortenerViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    @classmethod
    def setUpTestData(cls):
        cls.short1 = Shortener.get_or_create("Some sort of long query!")

    def test_create_invalid_request(self):
        with self.assertRaisesMessage(ValueError, "Only JSON POST requests are supported"):
            self.client.post(reverse('api:shortener-list'), data={})

    def test_create_empty(self):
        with self.assertRaisesMessage(ValueError,
                                      "Missing the field 'full' which should contain the full text to be shortened"):
            self.client.post(reverse('api:shortener-list'), data={}, content_type="application/json")

    def test_create_single(self):
        full = 'Some sort of long query, but different!'

        response = self.client.post(reverse('api:shortener-list'), data={'full': full}, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertNotEqual(data['id'], self.short1.id)
        self.assertEqual(data['full'], full)

    def test_create_multiple(self):
        fulls = ["query1", "query2"]

        response = self.client.post(reverse('api:shortener-list'), data={'full': fulls},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data[0]['full'], fulls[0])
        self.assertEqual(data[1]['full'], fulls[1])

    def test_retrieving_existing(self):
        response = self.client.post(reverse('api:shortener-list'), data={'full': self.short1.full},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data['id'], self.short1.id)


class metrics_passrate_trend_viewTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_basic(self):
        # TODO: Add a fixture that would return useful data here
        response = self.client.get(reverse('api-metrics-passrate-per-runconfig'),
                                   {'query': ""}, format='json')
        self.assertEqual(response.status_code, 200)


class metrics_passrate_viewTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_basic(self):
        # TODO: Add a fixture that would return useful data here
        response = self.client.get(reverse('api-metrics-passrate-per-test'),
                                   {'query': ""}, format='json')
        self.assertEqual(response.status_code, 200)
