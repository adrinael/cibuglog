from collections import namedtuple, OrderedDict
from rest_framework import serializers
from .models import Bug, Component, Build, Test, Machine, RunConfigTag, RunConfig, ReplicationScript
from .models import TestSuite, TextStatus, IssueFilter, MachineTag, BugTrackerAccount
from .models import Issue, BugTracker

from shortener.models import Shortener


class RunConfigTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = RunConfigTag
        fields = ('id', 'description', 'url', 'public', '__str__')
        read_only_fields = ('public', )


class TestSuiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSuite
        fields = ('id', '__str__')


class TestSerializer(serializers.ModelSerializer):
    testsuite = TestSuiteSerializer()

    class Meta:
        model = Test
        fields = ('id', 'name', 'testsuite', 'public', 'vetted_on', 'first_runconfig', '__str__')
        read_only_fields = ('public', )


class MachineTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = MachineTag
        fields = ('id', 'name', 'public')
        read_only_fields = ('added_on', )


class MachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Machine
        fields = ('id', 'public', 'vetted_on', '__str__')
        read_only_fields = ('public', )


class TextStatusSerializer(serializers.ModelSerializer):
    testsuite = TestSuiteSerializer()

    class Meta:
        model = TextStatus
        fields = ('id', 'name', 'testsuite', '__str__')


class IssueFilterSerializer(serializers.ModelSerializer):
    tags = RunConfigTagSerializer(many=True)
    tests = TestSerializer(many=True)
    machine_tags = MachineTagSerializer(many=True)
    machines = MachineSerializer(many=True)
    statuses = TextStatusSerializer(many=True)

    class Meta:
        model = IssueFilter
        fields = ('id', 'description', 'tags', 'machines', 'machine_tags', 'tests', 'statuses',
                  'stdout_regex', 'stderr_regex', 'dmesg_regex', '__str__')
        read_only_fields = ('added_on', )


class RunConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = RunConfig
        fields = ('id', 'name', 'tags', 'url', 'added_on', 'builds', 'environment', '__str__')
        read_only_fields = ('added_on', )


class ComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'name', 'description', 'url', 'public', '__str__')


class BuildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Build
        fields = ('id', 'name', 'component', 'version', 'added_on', 'parents',
                  'repo_type', 'branch', 'repo', 'upstream_url', 'parameters',
                  'build_log', '__str__')
        read_only_fields = ('id', 'added_on')


class BuildMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Build
        fields = ('id', 'name', 'added_on', 'parents', 'upstream_url', '__str__')
        read_only_fields = ('id', 'added_on')


class RunConfigResultsSerializer(serializers.Serializer):
    __str__ = serializers.CharField(max_length=255, read_only=True)
    is_failure = serializers.BooleanField(read_only=True)
    all_failures_covered = serializers.BooleanField(read_only=True)
    bugs_covering = serializers.SerializerMethodField()

    def get_bugs_covering(self, obj):
        ser = serializers.ListField(child=serializers.CharField(max_length=255, read_only=True))
        return ser.to_representation([b.short_name for b in obj.bugs_covering])


class RunConfigResultsDiffSerializer(serializers.Serializer):
    testsuite = serializers.SerializerMethodField()
    test = serializers.SerializerMethodField()
    machine = serializers.SerializerMethodField()
    result_from = RunConfigResultsSerializer(read_only=True)
    result_to = RunConfigResultsSerializer(read_only=True)

    def get_testsuite(self, obj):
        ser = serializers.CharField(max_length=255, read_only=True)
        return ser.to_representation(obj.testsuite.name)

    def get_test(self, obj):
        ser = serializers.CharField(max_length=255, read_only=True)
        return ser.to_representation(obj.test.name)

    def get_machine(self, obj):
        ser = serializers.CharField(max_length=255, read_only=True)
        return ser.to_representation(obj.machine.name)


class BugTrackerSerializer(serializers.ModelSerializer):
    class Meta:
        model = BugTracker
        fields = ('id', 'name', 'short_name', 'project', 'separator', 'url', 'tracker_type', 'polled',
                  'components_followed', 'components_followed_since', 'first_response_SLA')
        read_only_fields = ('id', 'name', 'short_name', 'project', 'separator', 'url', 'tracker_type', 'polled',
                            'components_followed', 'components_followed_since', 'first_response_SLA')


def serialize_bug(bug, new_comments=None):
    def _date_formatter(date_field):
        return str(date_field) if date_field is not None else None

    resp = {
        'url': bug.url,
        'bug_id': bug.bug_id,
        'title': bug.title,
        'description': bug.description,
        'tracker': str(bug.tracker),
        'created': _date_formatter(bug.created),
        'updated': _date_formatter(bug.updated),
        'polled': _date_formatter(bug.polled),
        'closed': _date_formatter(bug.closed),
        'creator': str(bug.creator),
        'assignee': str(bug.assignee),
        'product': bug.product,
        'component': bug.component,
        'priority': bug.priority,
        'severity': bug.severity,
        'features': bug.features_list,
        'platforms': bug.platforms_list,
        'status': bug.status,
        'tags': bug.tags_list,
        'custom_fields': bug.custom_fields,
        'new_comments': []
    }
    if new_comments:
        for comm in new_comments:
            person = comm.db_object.account.person
            author = person.full_name if person.full_name else person.email
            resp['new_comments'].append({'author': author,
                                         'created': str(comm.db_object.created_on),
                                         'body': comm.body})
    return resp


class BugCompleteSerializer(serializers.ModelSerializer):
    tracker = BugTrackerSerializer()

    class Meta:
        model = Bug
        fields = ('id', 'tracker', 'bug_id', 'parent', 'children', 'title', 'description', 'created',
                  'updated', 'polled', 'closed', 'creator', 'assignee', 'product', 'component',
                  'priority', 'severity', 'features', 'platforms', 'status', 'tags', 'comments_polled',
                  'flagged_as_update_pending_on', 'custom_fields')
        read_only_fields = ('id', 'tracker', 'bug_id', 'parent', 'children', 'title', 'description', 'created',
                            'updated', 'polled', 'closed', 'creator', 'assignee', 'product', 'component',
                            'priority', 'severity', 'features', 'platforms', 'status', 'tags', 'comments_polled',
                            'flagged_as_update_pending_on', 'custom_fields')


class BugMinimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bug
        fields = ('id', 'short_name', 'url')
        read_only_fields = ('id', 'short_name', 'url')


class ExecutionTimeSerializer(serializers.Serializer):
    minimum = serializers.DurationField(read_only=True)
    maximum = serializers.DurationField(read_only=True)
    count = serializers.IntegerField(read_only=True)


class RunConfigDiffSerializer(serializers.Serializer):
    runcfg_from = RunConfigSerializer(read_only=True)
    runcfg_to = RunConfigSerializer(read_only=True)
    results = RunConfigResultsDiffSerializer(read_only=True, many=True)
    new_tests = serializers.SerializerMethodField()
    builds = serializers.SerializerMethodField()
    bugs = BugMinimalSerializer(many=True)
    status = serializers.CharField(max_length=10, read_only=True)
    text = serializers.CharField(max_length=16000, read_only=True)

    class BuildDiff2Serializer(serializers.Serializer):
        component = ComponentSerializer(read_only=True)
        from_build = BuildMinimalSerializer(read_only=True)
        to_build = BuildMinimalSerializer(read_only=True)

    class RunConfigDiffNewTestsSerializer(serializers.Serializer):
        test = TestSerializer()
        statuses = serializers.DictField(child=serializers.IntegerField(read_only=True))
        exec_time = ExecutionTimeSerializer(read_only=True)

    def get_builds(self, obj):
        bd2 = namedtuple('BuildDiff2', ['component', 'from_build', 'to_build'])
        build_diffs = {k: bd2(k, v.from_build, v.to_build) for k, v in obj.builds.items()}
        dict_ser = serializers.DictField(child=self.BuildDiff2Serializer())
        return dict_ser.to_representation(build_diffs)

    def __statuses(self, statuses):
        return {k.name: v for k, v in statuses.items()}

    def get_new_tests(self, obj):
        NT = namedtuple('NewTest', ['test', 'statuses', 'exec_time'])
        new_tests = {k: NT(k, {k.name: v for k, v in v.to_statuses.items()}, v.to_exec_times)
                     for k, v in obj.new_tests.tests.items()}
        dict_ser = serializers.DictField(child=self.RunConfigDiffNewTestsSerializer())
        return dict_ser.to_representation(new_tests)


class ReplicationScriptSerializer(serializers.ModelSerializer):
    created_by = serializers.StringRelatedField()
    source_tracker = serializers.StringRelatedField()
    destination_tracker = serializers.StringRelatedField()

    class Meta:
        model = ReplicationScript
        fields = ('name', 'created_by', 'created_on', 'source_tracker', 'destination_tracker', 'script')


class KnownIssuesSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    testsuite = serializers.SerializerMethodField()
    machine = serializers.SerializerMethodField()
    run_id = serializers.SerializerMethodField()
    test = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()

    bugs = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Cache the serializers for performance reasons
        self._char_ser = serializers.CharField(max_length=255, read_only=True)
        self._int_ser = serializers.IntegerField(read_only=True)
        self._bug_min_ser = BugMinimalSerializer(many=True)

    def get_run_id(self, obj):
        return self._int_ser.to_representation(obj.result.ts_run.run_id)

    def get_testsuite(self, obj):
        return self._char_ser.to_representation(obj.result.test.testsuite.name)

    def get_test(self, obj):
        return self._char_ser.to_representation(obj.result.test.name)

    def get_machine(self, obj):
        return self._char_ser.to_representation(obj.result.ts_run.machine.name)

    def get_status(self, obj):
        return self._char_ser.to_representation(obj.result.status.name)

    def get_url(self, obj):
        return self._char_ser.to_representation(obj.result.url)

    def get_bugs(self, obj):
        return self._bug_min_ser.to_representation(obj.matched_ifa.issue.bugs.all())


class BugTrackerAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BugTrackerAccount
        fields = ('id', 'is_developer')
        read_only_fields = ('id', )


class ShortenerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shortener
        fields = ('id', 'shorthand', 'full', 'added_on', 'last_accessed')
        read_only_fields = ('id', 'shorthand', 'full', 'added_on', 'last_accessed')


class RateSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    total = serializers.IntegerField()
    percent = serializers.FloatField()


class BugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bug
        fields = ('short_name', 'title', 'url')
        read_only_fields = ('short_name', 'title', 'url')


class IssueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Issue
        fields = ('id', 'bugs', 'expected')
        read_only_fields = ('id', 'bugs', 'expected')

    bugs = BugSerializer(many=True)


class IssueSerializerMinimal(serializers.ModelSerializer):
    class Meta:
        model = Issue
        fields = ('id', )
        read_only_fields = ('id', )


def serialize_issue_hitrate(issues, minimal=False):
    Serializer = IssueSerializerMinimal if minimal else IssueSerializer

    ret = []
    for issue, rate in issues.items():
        val = Serializer(issue).data
        val['hit_rate'] = RateSerializer(rate).data
        ret.append(val)
    return ret


def serialize_MetricPassRatePerRunconfig(history):
    runconfigs = OrderedDict()
    for runconfig, _statuses in history.runconfigs.items():
        runconfigs[str(runconfig)] = OrderedDict()
        for status, rate in _statuses.items():
            runconfigs[str(runconfig)][str(status)] = RateSerializer(rate).data

    statuses = OrderedDict()
    for status, _runconfigs in history.statuses.items():
        statuses[str(status)] = OrderedDict()
        for runconfig, rate in _runconfigs.items():
            statuses[str(status)][str(runconfig)] = RateSerializer(rate).data

    return {
        "runconfigs": runconfigs,
        "statuses": statuses,
        "most_hit_issues": serialize_issue_hitrate(history.most_hit_issues),
        "query_key":  history.query.query_key,
    }


class PassRateStatisticsSerializer(serializers.Serializer):
    passrate = RateSerializer()
    runrate = RateSerializer()
    discarded_rate = RateSerializer()
    notrun_rate = RateSerializer()


def serialize_MetricPassRatePerTest(metric_passrate):
    discarded_status = "discarded (expected)"

    tests = OrderedDict()
    for test, results in metric_passrate.tests.items():
        if results.is_fully_discarded:
            tests[str(test)] = {
                "status": discarded_status,
                "is_pass": False,
                "is_run": False,
                "duration": str(results.duration),
                "issues_hit": serialize_issue_hitrate(results.issue_occurence_rates, minimal=True),
            }
        else:
            tests[str(test)] = {
                "status": str(results.overall_result),
                "is_pass": results.is_pass,
                "is_run": not results.overall_result.is_notrun,
                "duration": str(results.duration),
                "issues_hit": serialize_issue_hitrate(results.issue_occurence_rates, minimal=True),
            }

    statuses = OrderedDict()
    for status, rate in metric_passrate.statuses.items():
        statuses[str(status)] = RateSerializer(rate).data

    return {
        "tests": tests,
        "statuses": statuses,
        "discarded_status": discarded_status,
        "machines": [str(m) for m in metric_passrate.machines],
        "runconfigs": RunConfigSerializer(metric_passrate.runconfigs, many=True).data,
        "raw_statistics": PassRateStatisticsSerializer(metric_passrate.raw_statistics).data,
        "statistics": PassRateStatisticsSerializer(metric_passrate.statistics).data,
        "most_hit_issues": serialize_issue_hitrate(metric_passrate.most_hit_issues),
        "uncovered_failure_rate": RateSerializer(metric_passrate.uncovered_failure_rate).data,
        "notrun_rate": RateSerializer(metric_passrate.notrun_rate).data,
        "most_interrupting_issues": serialize_issue_hitrate(metric_passrate.most_interrupting_issues),
        "unknown_failure_interruption_rate": RateSerializer(metric_passrate.unknown_failure_interruption_rate).data,
        "unexplained_interruption_rate": RateSerializer(metric_passrate.unexplained_interruption_rate).data,
        "query_key":  metric_passrate.query.query_key,
    }
