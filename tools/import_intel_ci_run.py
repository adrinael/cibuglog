#!/usr/bin/env python3

from pathlib import Path
import urllib.request
import requests
import argparse
import sys
import re
import os

import django
from django.utils.functional import cached_property

sys.path.append(os.path.abspath('{}/../'.format(os.path.dirname(__file__))))
django.setup()

from CIResults.run_import import BuildResult, RunConfigResults, TestSuiteRunDef  # noqa
from CIResults.models import Build, TestSuite, TextStatus, Component, RunConfigTag

class CIRunImport:
    def get_file(self, url):
        r = requests.get(url)
        if r.status_code == 200:
            return r.text
        else:
            return None

    def save_file(self, url, filepath):
        cached_path_dir = Path.home() / ".cache" / "CI_run" / self.runcfg
        cached_path_dir.mkdir(parents=True, exist_ok=True)

        cached_path = cached_path_dir / filepath.replace("/", "_")
        if cached_path.exists():
            return cached_path

        print("Downloading {}".format(filepath))
        urllib.request.urlretrieve(url, cached_path)

        return cached_path

    def file_url(self, path):
        return "{}/{}/{}".format(self.base_url, self.runcfg, path)

    @cached_property
    def result_urls(self):
        filelist = self.get_file(self.file_url("filelist.txt"))
        if filelist is None:
            print("ERROR: cannot find the file filelist.txt", file=sys.stderr)
            return []

        files = []
        for line in filelist.splitlines():
            size, path = line.split(' ')

            if ".json.bz2" in path:
                files.append((path, self.file_url(path)))
        return files

    def component_from_build_name(self, build_name):
        if build_name.startswith('CI_DRM_'):
            return "Linux"
        elif build_name.startswith('IGT_'):
            return "IGT"
        elif build_name.startswith('piglit_'):
            return "Piglit"
        elif build_name.startswith('CI_'):
            return "CI"
        return None

    def __get_or_create(self, model, search_kwargs, set_kwargs={}):
        try:
            obj = model.objects.get(**search_kwargs)
            for key, value in set_kwargs.items():
                setattr(obj, key, value)
            obj.save()
            return obj
        except Exception:
            return model.objects.create(**search_kwargs, **set_kwargs)

    @cached_property
    def builds(self):
        lines = self.get_file(self.file_url("components.txt"))
        if lines is None:
            print("ERROR: cannot find the file components.txt", file=sys.stderr)
            return {}

        components = dict()
        for line in lines.splitlines():
            version, build_name = line.split(' ')

            component = self.component_from_build_name(build_name)
            if component is None:
                continue

            build = BuildResult(name=build_name, component=component, version=version)
            build.commit_to_db()

            components[component] = build_name

        # HACK: This is sometimes missing and not super important, so let's just use a generic one
        if 'CI' not in components:
            components['CI'] = Build.objects.filter(component__name='CI').order_by('-added_on').first()
            if components['CI'] is None:
                CI = Component.objects.get(name='CI')
                components['CI'] = Build.objects.create(component=CI, name='unspecified', version='unspecified')

        # HACK: As of 2020-01-20, Intel CI fails to show the IGT version for IGT pre-merge testing,
        # but luckily we can infer it from the name
        if 'IGT' not in components and self.runcfg.startswith('IGTPW_'):
            build = BuildResult(name=self.runcfg, component='IGT', version=self.runcfg)
            build.commit_to_db()
            components['IGT'] = self.runcfg

        return components

    def __setup_components(self):
        # Add all the components
        self.__get_or_create(Component, search_kwargs={'name': 'Linux'}, set_kwargs={'public': True})

        # Add the test suites
        igt = self.__get_or_create(TestSuite, search_kwargs={'name': 'IGT'}, set_kwargs={'public': True})
        igt_notrun = self.__get_or_create(TextStatus, search_kwargs={'name': 'notrun', 'testsuite': igt},
                                          set_kwargs={'severity': 0, 'color_hex': '#e0e0e0'})
        igt_pass = self.__get_or_create(TextStatus, search_kwargs={'name': 'pass', 'testsuite': igt},
                                        set_kwargs={'severity': 1, 'color_hex': '#30ff30'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'skip', 'testsuite': igt},
                             set_kwargs={'severity': 2, 'color_hex': '#b0b0b0'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'warn', 'testsuite': igt},
                             set_kwargs={'severity': 10, 'color_hex': '#ff9020'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'dmesg-warn', 'testsuite': igt},
                             set_kwargs={'severity': 20, 'color_hex': '#df7000'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'fail', 'testsuite': igt},
                             set_kwargs={'severity': 30, 'color_hex': '#ff2020'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'dmesg-fail', 'testsuite': igt},
                             set_kwargs={'severity': 40, 'color_hex': '#ba0000'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'timeout', 'testsuite': igt},
                             set_kwargs={'severity': 50, 'color_hex': '#83bdf6'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'crash', 'testsuite': igt},
                             set_kwargs={'severity': 60, 'color_hex': '#111111'})
        self.__get_or_create(TextStatus, search_kwargs={'name': 'incomplete', 'testsuite': igt},
                             set_kwargs={'severity': 70, 'color_hex': '#853385'})
        igt.acceptable_statuses.add(igt_pass, igt_notrun)
        igt.notrun_status = igt_notrun
        igt.save()

        piglit = self.__get_or_create(TestSuite, search_kwargs={'name': 'Piglit'}, set_kwargs={'public': True})
        piglit_pass = self.__get_or_create(TextStatus, search_kwargs={'name': 'pass', 'testsuite': piglit},
                                           set_kwargs={'severity': 1, 'color_hex': '#30ff30'})
        piglit.acceptable_statuses.add(piglit_pass)

        CI = self.__get_or_create(TestSuite, search_kwargs={'name': 'CI'}, set_kwargs={'public': True})
        CI_pass = self.__get_or_create(TextStatus, search_kwargs={'name': 'pass', 'testsuite': CI},
                                       set_kwargs={'severity': 1, 'color_hex': '#30ff30'})
        CI.acceptable_statuses.add(CI_pass)

        # Create the DRM-TIP runconfig tag
        self.__get_or_create(RunConfigTag, search_kwargs={'name': 'DRM-TIP'}, set_kwargs={'public': True})


    def __init__(self, runcfg, base_url, result_url_pattern):
        self.runcfg = runcfg
        self.base_url = base_url
        self.result_url_pattern = result_url_pattern

        self.__setup_components()

        outputs = dict()
        for path, url in self.result_urls:
            outputs[path] = self.save_file(url, path)

        bat_results = []
        full_results = []
        for path, filepath in outputs.items():
            m = re.match("(?P<machine>.*)/(?P<testsuite>[^0-9]+)(?P<run_id>\d+).json.bz2", path)
            if m:
                machine = m.group('machine')
                testsuite = m.group('testsuite')
                run_id = m.group('run_id')

                if testsuite == 'boot':
                    build = self.builds['CI']
                elif machine.startswith('pig-'):
                    if testsuite != 'failed_results':  # only import failures to keep storage needs low
                        continue
                    build = self.builds['Piglit']
                else:
                    build = self.builds['IGT']

                ts_run = TestSuiteRunDef(testsuite_build=build, results_format="piglit",
                                         results_format_version=1, machine=machine, ts_run_id=run_id, ts_run_path=filepath)

                if machine.startswith('fi-'):
                    bat_results.append(ts_run)
                else:
                    full_results.append(ts_run)

        temporary = not (self.runcfg.startswith('CI_DRM_') or self.runcfg.startswith('IGT_'))
        bat = RunConfigResults(name=self.runcfg, result_url_pattern=args.result_url_pattern,
                               builds=self.builds.values(), tags=["DRM-TIP"], results=bat_results,
                               temporary=temporary)
        bat.commit_to_db()

        full = RunConfigResults(name=self.runcfg+"_full", result_url_pattern=args.result_url_pattern,
                                builds=self.builds.values(), tags=["DRM-TIP"], results=full_results,
                                temporary=temporary)
        full.commit_to_db()

# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("runcfg", help="Name of the runconfig to import")
parser.add_argument("--base_url", help="Base URL for the filelight",
                    default="https://intel-gfx-ci.01.org/tree/drm-tip")
parser.add_argument("--result_url_pattern", help="See add_run.py's documentation",
                    default="https://intel-gfx-ci.01.org/tree/drm-tip/{runconfig}/{machine}/{test}.html")

args = parser.parse_args()

run = CIRunImport(args.runcfg, args.base_url, args.result_url_pattern)
